"""
Controls the ATT&CK navigator display.
"""

import logging
from json import dump as json_dump, load as json_load
from os.path import join as path_join, exists as path_exists
from os import unlink, remove, rename
from shutil import copyfile, copytree, rmtree, unpack_archive
from irca_layer import create_empty_layer # pylint: disable=import-error


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("IRCA")

ENTERPRISE_STIX_FILENAME = "assets/enterprise-attack.json"
MOBILE_STIX_FILENAME = "assets/mobile-attack.json"
ICS_STIX_FILENAME = "assets/ics-attack.json"
STIX_PATH = '/local-stix-data/'

def get_default_attack_layers(assets_path, config_file="config.json"):
    """Get the paths of the default layer files currently being used in the
    navigator."""
    with open(path_join(assets_path, config_file), 'r', encoding='utf-8') as current_file:
        data = json_load(current_file)
        return data["default_layers"]["urls"] if \
               data["default_layers"]["enabled"] else []


def set_default_attack_layers(urls, assets_path, config_file="config.json"):
    """Set the path containing the configuration JSON that defines the layer to
    be displayed when opening the ATT&CK navigator.
    """
    # sets the default layer(s) to be loaded when attack navigator is opened
    # assumes default layers have been placed in the nav-app/src/assets/ directory
    # urls should be a list of strings of layers to be loaded with assets/ prepended
    # ex: ['assets/default-layer.json']

    # load the current src/assets/config.json
    with open(path_join(assets_path, config_file), 'r', encoding='utf-8') as current_file:
        data = json_load(current_file)

        # edit default_layers in src/assets/config.json to true
        data["default_layers"]["enabled"] = True

        # add the paths to the desired default layers to the urls array in default_layers
        data["default_layers"]["urls"] = urls

    # write the updated config.json
    with open(path_join(assets_path, config_file), 'w', encoding='utf-8') as current_file:
        json_dump(data, current_file)


def add_default_attack_layers(urls, assets_path, config_file="config.json"):
    """Adds one or more paths to the ATT&CK navigator configuration JSON."""
    current_urls = get_default_attack_layers(assets_path)
    for url in urls:
        if url not in current_urls:
            current_urls.append(url)
    set_default_attack_layers(current_urls, assets_path, config_file)


def remove_default_attack_layer(url, assets_path, config_file="config.json"):
    """Removes a path from ATT&CK navigator configuration JSON."""
    remove_success = False
    try:

        current_urls = get_default_attack_layers(assets_path)
        if url in current_urls:
            current_urls.remove(url)
        set_default_attack_layers(current_urls, assets_path, config_file)
        remove_success = True
    # pylint: disable=broad-except
    except Exception as error:
        print(error)
        remove_success = False
    return remove_success


def disable_default_attack_layers(assets_path, config_file="config.json"):
    """Set the path containing the configuration JSON that defines the layer to
    be displayed when opening the ATT&CK navigator.
    """
    # disables the default layer(s) to be loaded when attack navigator is opened

    # load the current src/assets/config.json
    with open(path_join(assets_path, config_file), 'r', encoding='utf-8') as current_file:
        data = json_load(current_file)

        # edit default_layers in src/assets/config.json to true
        data["default_layers"]["enabled"] = False

    # write the updated config.json
    with open(path_join(assets_path, config_file), 'w', encoding='utf-8') as current_file:
        json_dump(data, current_file)


def set_stix(assets_path, domain):
    """Set the domain STIX file for ATT&CK v11.
    Retuns if setting was successful"""
    logger.warning("setting stix data")
    # remove temp_data
    try:
        logger.warning("removing temp stix data")
        rmtree(STIX_PATH + 'temp_data/')
    except OSError:
        pass
    logger.warning("unpacking stix data")
    # unzip the STIX data a look for the correct domain bundle file
    unpack_archive(STIX_PATH + f'{domain}-attack.zip', STIX_PATH + 'temp_data/', 'zip')
    if not path_exists(STIX_PATH + 'temp_data/' + f'{domain}-attack.json') and \
        not path_exists(STIX_PATH + 'temp_data/' + f'{domain}-attack/' + f'{domain}-attack.json'):
        rmtree(STIX_PATH + 'temp_data/')
        logger.warning("temp_data/%s-attack.json does not exists", domain)
        return False
    logger.warning("checking for bundle stix data")
    # change name of default STIX folder if it has not been saved yet
    if not path_exists(STIX_PATH + f'{domain}-attack-default-stix'):
        copytree(STIX_PATH + f'{domain}-attack', \
            STIX_PATH + f'{domain}-attack' + '-default-stix')
    logger.warning("unlink bundle stix data")
    # remove the old STIX bundle file
    try:
        unlink(assets_path + f'{domain}-attack.json')
    except FileNotFoundError:
        logger.warning("remove old folder stix data")
    # remove the old STIX folder
    try:
        rmtree(STIX_PATH + f'{domain}-attack/')
    except OSError:
        return False
    logger.warning("unpacking again stix data")
    # unzip and write STIX folder
    unpack_archive(STIX_PATH + f'{domain}-attack.zip', STIX_PATH + f'{domain}-attack/', 'zip')
    logger.warning("copy bundle stix data")
    # copy STIX bundle to spa/build/assets
    if path_exists(STIX_PATH + 'temp_data/' + f'{domain}-attack.json'):
        copyfile(STIX_PATH + f'{domain}-attack/{domain}-attack.json', \
            assets_path + f'{domain}-attack.json')
    elif path_exists(STIX_PATH + 'temp_data/' + f'{domain}-attack/' + f'{domain}-attack.json'):
        # remove the extra folder
        rename(STIX_PATH + f'{domain}-attack', STIX_PATH + f'{domain}-attack-c')
        copytree(STIX_PATH + f'{domain}-attack-c/{domain}-attack', STIX_PATH + f'{domain}-attack')
        rmtree(STIX_PATH + f'{domain}-attack-c')
        copyfile(STIX_PATH + f'{domain}-attack/{domain}-attack.json', \
            assets_path + f'{domain}-attack.json')
    else:
        logger.warning("%s-attack.json does not exists", domain)
        return False
    logger.warning("Adding STIX data to default techniques")
    logger.warning("**************************************")
    create_empty_layer()
    return True

def reset_stix(assets_path):
    """Resets the domain STIX files for ATT&CK v11 to their default"""
    domains = ['enterprise', 'mobile', 'ics']
    for dom in domains:
        # only rest the domains that have been modified
        if path_exists(STIX_PATH + f'{dom}-attack-default-stix'):
            # remove the old STIX folder
            try:
                rmtree(STIX_PATH + f'{dom}-attack/')
            except OSError:
                return False

            # remove the old STIX bundle file
            try:
                remove(assets_path + f'{dom}-attack.json')
            except FileNotFoundError:
                pass

            # change name of default STIX folders
            copytree(STIX_PATH + f'{dom}-attack-default-stix', \
                STIX_PATH + f'{dom}-attack')

            # copy STIX bundles to spa/build/assets
            copyfile(STIX_PATH + f'{dom}-attack/{dom}-attack.json', \
                assets_path + f'{dom}-attack.json')

    return True
