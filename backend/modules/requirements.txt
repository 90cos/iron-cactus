elasticsearch==7.9.1
opensearch-py==2.2.0
pandas==2.0.2
requests==2.31.0
pysigma==0.9.11
pysigma-backend-elasticsearch==1.0.3
stix2==3.0.1