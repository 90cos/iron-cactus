"""
Contains wrapper functions for DeTT&CT's CLI code.
"""
# pylint: disable=duplicate-code

from logging import getLogger, ERROR as LOGERROR
from os.path import join as path_join
from sys import path as sys_path

# pylint: disable=import-error
def finish_dettect_import(dettect_path):
    """Cleans up system PATH after importing DeTT&CT code."""
    sys_path.remove(dettect_path)

def start_dettect_import(dettect_path, local_stix_path):
    """Adds DeTT&CT code to PATH for importing purposes."""
    sys_path.append(dettect_path)
    # pylint: disable=import-outside-toplevel
    try:
        getLogger("taxii2client").setLevel(LOGERROR)
        import generic
        generic.local_stix_path = path_join(local_stix_path)
    except ImportError as error:
        print("Failed to load DeTT&CT Python code!")
        finish_dettect_import(dettect_path)
        raise error

def group_list_to_layer(dettect_path, groups, layer_name,
                        local_stix_path="local-stix-data"):
    """Invoke DeTT&CT's code to get a JSON string from a threat group list."""
    # pylint: disable=too-many-locals
    # Get DeTT&CT Python code
    start_dettect_import(dettect_path, local_stix_path)
    # pylint: disable=import-outside-toplevel
    from constants import PLATFORMS_ENTERPRISE
    from generic import platform_to_name
    from group_mapping import _get_group_list, _get_group_techniques, \
        _get_technique_count, _get_technique_layer
    from navigator_layer import get_layer_template_groups
    finish_dettect_import(dettect_path)

    overlay_type = "group"
    platform = list(PLATFORMS_ENTERPRISE.values())
    domain = "enterprise-attack"
    layer_settings = {}
    overlay_dict = {}
    groups_dict = {}

    if groups is None:
        groups = ['all']
    else:  # reached when the groups are provided via CLI arguments
        groups = list(map(lambda x: x.strip().lower(), groups))

    for group in groups:
        group_dict =_get_group_techniques([group], platform, None, domain)
        # check if Group is found in ATT&CK, returns -1 if not
        if group_dict != -1:
            groups_dict = groups_dict | group_dict

    if len(groups_dict) == 0:
        print('[!] Empty layer.')  # provided groups did not result in
        return None  # any techniques


    technique_count, max_count = _get_technique_count(groups_dict, {}, {},
                                                      overlay_type, None)
    technique_layer = _get_technique_layer(technique_count, groups_dict, {},
                                           {}, None, overlay_type, None, False,
                                           {})

    # make a list group names for the involved groups.
    if groups == ['all']:
        groups_list = ['all']
    else:
        groups_list = _get_group_list(groups_dict, None)
    overlay_list = _get_group_list(overlay_dict, None)

    desc = f"stage: attack | platform(s): " \
           f"{platform_to_name(platform, domain, separator=', ')}" \
           f" | group(s): {', '.join(groups_list)} | overlay group(s): " \
           f"{', '.join(overlay_list)}"

    layer = get_layer_template_groups(layer_name, max_count, desc, platform,
                                      overlay_type, domain, layer_settings)
    for technique in technique_layer:
        technique["comment"] = technique.get("comment", "")
        technique["enabled"] = True
        technique["links"] = []
        technique["score"] = technique.get("score", 0) * 100
    layer["gradient"]["maxValue"] = 100
    layer['techniques'] = technique_layer
    return layer
