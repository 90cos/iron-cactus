"""
Functions to help create the dictionary required for DeTT&CT input.
"""


# SYSTEMS
def get_system(dettect_yaml, name):
    """Get the dictionary for a host/system, creating one if necessary."""
    for system in dettect_yaml["systems"]:
        if system["applicable_to"] == name:
            return system
    system = {
        "applicable_to": name,
        "platform": ["all"]
    }
    dettect_yaml["systems"].append(system)
    return system


def add_platform(system, platform):
    """Adds a platform to the specified system."""
    platforms = system["platform"]
    if platforms == ["all"]:
        platforms.clear()
    if platform not in platforms:
        platforms.append(platform)
        return True
    return False


# DATA SOURCES
def get_data_source(dettect_yaml, name):
    """Get the data source dictionary for a specified data source type,
    creating it if necessary.
    """
    for data_entry in dettect_yaml["data_sources"]:
        if data_entry["data_source_name"] == name:
            return data_entry["data_source"]
    data_entry = {
        "data_source_name": name,
        "data_source": []
    }
    dettect_yaml["data_sources"].append(data_entry)
    return data_entry["data_source"]


def dq_clamp(metric):
    """Clamp metric between 0 and 5, both inclusive."""
    return min(max(metric, 0), 5)


# pylint: disable=too-many-arguments
def add_applicable_to(data_source, applicable_to: list, date_registered=None,
    date_connected=None, products: list=None,
    available_for_data_analytics: bool=None, comment: str="",
    device_completeness: int=0, data_field_completeness: int=0,
    timeliness: int=0, consistency: int=0, retention: int=0):
    """Adds a list of applicable systems to a data source."""
    applicable = {
        "applicable_to": applicable_to,
        "date_registered": date_registered,
        "date_connected": date_connected,
        "products": products or [],
        "available_for_data_analytics": available_for_data_analytics,
        "comment": comment,
        "data_quality": {
            "device_completeness": dq_clamp(device_completeness),
            "data_field_completeness": dq_clamp(data_field_completeness),
            "timeliness": dq_clamp(timeliness),
            "consistency": dq_clamp(consistency),
            "retention": dq_clamp(retention)
        }
    }
    data_source.append(applicable)
    return applicable
