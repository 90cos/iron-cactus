"""
Houses Elastic wrapper functions.
"""
# pylint: disable=import-error
from logging import info as log_info

from elasticsearch import Elasticsearch
from opensearchpy import RequestsHttpConnection

def connect_to_elastic(hostname, hostport, use_ssl, username, password):
    # pylint: disable=broad-except
    """Returns an Elasticsearch instance if successful."""
    try:
        log_info("Trying %s:%s", hostname, hostport)
        # Read the properties file.
        elastic = Elasticsearch(
            hosts=[{'host': hostname,
                    'port': hostport}],
            max_retries=30,
            use_ssl=use_ssl,
            retry_on_timeout=True,
            request_timeout=30,
            verify_certs=False,
            ssl_show_warn=False,
            connection_class=RequestsHttpConnection,
            http_auth=(username, password)
            )
        # Check if the connection is successful.
        if elastic.ping():
            log_info("Elasticsearch cluster at %s:%s is up.",
                     hostname, hostport)
        else:
            log_info("Elasticsearch cluster at %s:%s is down.",
                     hostname, hostport)
    except Exception as error:
        log_info("Error: %s", error)
        raise error
    return elastic

def search_has_results(elastic_obj, dsl_query, index_name="_all"):
    """
    Perform an elasticsearch search, if there is more than 0 results
    for the query, return the number of hits and the
    _id of the first one, else None.

    Lastly, if the result returned more than one hit AND the source index came
    from a built-in '.kibana' index, ignore it.
    """
    result = elastic_obj.search(index=index_name, body=dsl_query)
    num_hits = result["hits"]["total"]["value"]
    doc_id = None

    if num_hits != 0:
        if result['hits']['hits'][0]['_index'].startswith('.kibana'):
            num_hits = 0
            doc_id = None
        else:
            doc_id = result["hits"]["hits"][0]["_id"]

    return num_hits, doc_id
