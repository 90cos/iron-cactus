"""
Deals with DeTT&CT data source YAML content.
"""

# pylint: disable=import-error
from irca_dettect import start_dettect_import, finish_dettect_import

def rescore_ds_techniques(techniques):
    """Averages reported scores from DeTT&CT's data source score."""
    for technique in techniques:
        total = 0
        sources = 0
        for metadata in technique.get("metadata", []):
            if metadata.get("name") == "Score":
                total += int(metadata["value"][:-1])
                sources += 1
        score = 0
        if sources > 0:
            score = int(total / sources)
        technique["color"] = ""
        technique["enabled"] = score > 0
        technique["score"] = score
    return techniques

def create_data_source_layer(dsyaml, dettect_path, local_stix_path):
    """Invoke DeTT&CT's code to convert a dictionary
    to a JSON string usable by ATT&CK Navigator.
    """
    # pylint: disable=too-many-locals
    # Get DeTT&CT Python code
    start_dettect_import(dettect_path, local_stix_path)
    # pylint: disable=import-outside-toplevel
    from data_source_mapping import load_data_sources, chain, \
        get_layer_template_data_sources
    from data_source_mapping import _map_and_colorize_techniques
    finish_dettect_import(dettect_path)

    # Run YAML to JSON
    # my_data_sources, name, systems, exceptions, domain = \
    my_data_sources, layer_name, systems, exceptions, domain = \
        load_data_sources(dsyaml)
    my_techniques = _map_and_colorize_techniques(my_data_sources, systems,
        exceptions, domain)
    platforms = list(
        set(chain.from_iterable(map(lambda k: k['platform'], systems))))
    layer = get_layer_template_data_sources(layer_name, 'description',
        platforms, domain, {"showAggregateScores": "false"})
    layer["gradient"] = {
        "colors": ["#00000000", "#8888ffff"],
        "minValue": 0,
        "maxValue": 100
    }
    layer["hideDisabled"] = True
    layer["techniques"] = rescore_ds_techniques(my_techniques)
    return layer
