"""
Creates ATT&CK layers from threat group lists.
"""
# pylint: disable=duplicate-code

from json import dump as json_dump
from logging import getLogger
from os.path import join as path_join

# pylint: disable=import-error
from irca_attack import add_default_attack_layers
from irca_dettect import group_list_to_layer, start_dettect_import, \
                         finish_dettect_import
from irca_layer import create_empty_layer, make_technique_dict, technique_key


ASSETS_PATH = './spa/build/assets/'
RESOURCES_PATH = 'backend/resources'

logger = getLogger("IRCA_GROUPS")

def create_group_layer(gryaml, dettect_path, local_stix_path):
    """
    Takes a Dettect group file as input, converts it to an Attack-
    Navigator layer file in json format, saves that file to the UPLOADS_
    FOLDER and removes the yaml file.
    """
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-statements
    # Get DeTT&CT Python code
    start_dettect_import(dettect_path, local_stix_path)
    # pylint: disable=import-outside-toplevel
    from generic import platform_to_name
    from group_mapping import _get_group_list, _get_group_techniques, \
        _get_software_techniques, _get_technique_count, \
        _get_technique_layer
    from navigator_layer import get_layer_template_groups
    finish_dettect_import(dettect_path)

    groups_dict = {}
    overlay_dict = {}
    groups_software_dict = {}
    groups_file_type = 'group-administration'
    overlay_file_type = None

    groups = gryaml['groups']
    for group in groups:
        if len(group['technique_id']) > 0:
            if group['enabled']:
                campaign = group.get('campaign', None)
                campaign = str(campaign) if campaign else ''
                group_id = group['group_name']
                groups_dict[group_id] = {}

            groups_dict[group_id]['group_name'] = str(group['group_name'])
            if isinstance(group['technique_id'], list):
                groups_dict[group_id]['techniques'] = set(
                    group['technique_id'])
                groups_dict[group_id]['weight'] = dict(
                    (i, 1) for i in group['technique_id'])
            elif isinstance(group['technique_id'], dict):
                groups_dict[group_id]['techniques'] = set(
                    group['technique_id'].keys())
                groups_dict[group_id]['weight'] = group['technique_id']
            if campaign != '':
                groups_dict[group_id]['campaign'] = str(campaign)
            groups_dict[group_id]['software'] = group.get('software_id', None)

    if len(groups_dict) > 0:
        domain = 'enterprise-attack' if 'domain' not in gryaml.keys(
        ) else gryaml['domain']
        groups_list = _get_group_list(groups_dict, groups_file_type)
        overlay_list = _get_group_list(overlay_dict, overlay_file_type)
        platform = gryaml['platform']
        overlay_type = 'group'
        all_techniques = None
        groups_software_dict = _get_software_techniques(
            groups, platform, domain)
        technique_count, max_count = _get_technique_count(
            groups_dict, overlay_dict, groups_software_dict, overlay_type, all_techniques)
        technique_layer = _get_technique_layer(technique_count, groups_dict, overlay_dict, \
            groups_software_dict, overlay_file_type, overlay_type, all_techniques, False, {})
        desc = 'stage: attack | platform(s): ' + platform_to_name(platform, domain, separator=', ')\
            + ' | group(s): ' + ', '.join(groups_list) + ' | overlay group(s): ' + \
            ', '.join(overlay_list)
        layer_name = 'Attack_' + \
            platform_to_name(platform, domain, separator='_')
        layer = get_layer_template_groups(
            layer_name, max_count, desc, platform, overlay_type, domain, {})
        for technique in technique_layer:
            technique["comment"] = technique.get("comment", "")
            technique["enabled"] = True
            technique["links"] = []
            technique["score"] = technique.get("score", 0) * 100
        layer["gradient"]["maxValue"] = 100
        layer['techniques'] = technique_layer

        # Disable all unused techniques.
        layer["hideDisabled"] = True
        layer["gradient"]["colors"] = ["#ffcccc88", "#ff8888ff"]
        layer["legendItems"][0]["color"] = "#ffcccc88"
        layer["legendItems"][1]["color"] = "#ff8888ff"
        group_layer_t = make_technique_dict(layer)
        empty_layer = create_empty_layer()
        for technique in empty_layer["techniques"]:
            if technique_key(technique) not in group_layer_t:
                technique_layer.append(technique)

        return layer
    logger.info('No valid groups found')
    return None

def create_threat_group_layer(layer_name, group_list, dettect_path="."):
    """Creates and adds a threat group layer to the current navigator."""
    group_layer = group_list_to_layer(dettect_path, group_list, layer_name)
    if not group_layer:
        return {'result': 'Unable to create layer. Invalid threat group or \
            no techniques found for threat group.'}, 503
    group_layer["hideDisabled"] = True
    group_layer["gradient"]["colors"] = ["#ffcccc88", "#ff8888ff"]
    group_layer["legendItems"][0]["color"] = "#ffcccc88"
    group_layer["legendItems"][1]["color"] = "#ff8888ff"
    group_layer_t = make_technique_dict(group_layer)
    techniques_layer = create_empty_layer()
    # Disable all unused techniques.
    for technique in techniques_layer["techniques"]:
        if technique_key(technique) not in group_layer_t:
            group_layer["techniques"].append(technique)
    file_name = f"{layer_name}.json"
    with open(path_join(ASSETS_PATH, file_name), 'w', encoding="utf-8") \
            as layer_file:
        json_dump(group_layer, layer_file, indent=2)
    add_default_attack_layers([f"assets/{file_name}"], ASSETS_PATH)
    return {'result': 'success'}, 200

def get_threat_groups(dettect_path, local_stix_data):
    """Returns a json list of {"name": "APTNAME", "id": "APTID"}
    for all threat groups"""
    # load the list of groups
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-statements
    # Get DeTT&CT Python code
    start_dettect_import(dettect_path, local_stix_data)
    # pylint: disable=import-outside-toplevel
    from generic import load_attack_data
    finish_dettect_import(dettect_path)

    # use dettect's load_attack_data() to get group data
    data = load_attack_data('mitre_all_groups')

    threats = {}
    for group in data:
        threats.setdefault(group["id"], set()).add(group["name"])

    threats = [{"name": gn, "id": gid} for gid, gs in threats.items() for gn in gs]
    threats.sort(key=lambda d: d["name"])
    return threats
