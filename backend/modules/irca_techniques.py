"""
Deals with DeTT&CT technique YAML content.
"""

# pylint: disable=import-error
from irca_dettect import start_dettect_import, finish_dettect_import

def rescore_t_techniques(techniques):
    """Multiplies scores by 25 to standardize gradient to 0-100."""
    for technique in techniques:
        # DeTT&CT thinks it's funny to put None in score.
        score = technique.get("score") or 0
        technique["color"] = ""
        technique["enabled"] = score > 0
        technique["score"] = score * 25
    return techniques

def create_technique_layer(tayaml, dettect_path, local_stix_path):
    """
    Takes DeTT&CT YAML content (dict) as input, converts it to an
    Attack-Navigator layer file in json format, and saves that file to
    the UPLOADS_FOLDER.
    """
    # pylint: disable=too-many-locals
    start_dettect_import(dettect_path, local_stix_path)
    # pylint: disable=import-outside-toplevel
    from constants import COLOR_V_1, COLOR_V_2, COLOR_V_3, COLOR_V_4
    from generic import load_techniques
    from technique_mapping import get_layer_template_visibility, \
        _set_platform, _map_and_colorize_techniques_for_visibility
    finish_dettect_import(dettect_path)

    my_techniques, layer_name, platform_yaml, domain = load_techniques(tayaml)
    platform = _set_platform(platform_yaml, 'None', domain)
    techniques = _map_and_colorize_techniques_for_visibility(
        my_techniques, platform, domain)
    visibility_layer = get_layer_template_visibility(
        layer_name, 'description', platform, domain, {})
    visibility_layer["gradient"] = {
        "colors": ["#00000000", COLOR_V_1, COLOR_V_2, COLOR_V_3, COLOR_V_4],
        "minValue": 0,
        "maxValue": 100
    }
    visibility_layer["hideDisabled"] = True
    visibility_layer['techniques'] = rescore_t_techniques(techniques)
    return visibility_layer
