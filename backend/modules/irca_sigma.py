'''
module responsible for utilizing the Sigma library to validate,
convert, and process rules
'''

# pylint: disable=import-error
# pylint: disable=too-few-public-methods

import logging
from sigma.collection import SigmaCollection
from sigma.backends.elasticsearch import LuceneBackend
from sigma.exceptions import SigmaConditionError
from sigma.rule import SigmaRule

class SigmaToElastic:
    """
    simple wrapper class
    """
    def __init__(self, sigma_rule : SigmaRule, dsl_lucene_query: dict):
        self.src_sigma_rule = sigma_rule
        self.dsl_lucene_query = dsl_lucene_query

    def __str__(self):
        my_str = f'src_sigma_rule_id: {self.src_sigma_rule.id}\n'
        my_str += f'dsl_lucene_query: {self.dsl_lucene_query}\n'
        return my_str

def extract_technique_ids_from_sigma_rule(rule : SigmaRule) -> list:
    """
    returns a list of properly formatted mitre attach technique ID strings

    only valid techniques are:
    a. TNNNN,        where N is an integer
    b. TNNNN.NNN     where N is an integer

    RULE EXAMPLE

    ...snip
    tags:
    - attack.exfiltration
    - attack.t1048.003
    - attack.command_and_control
    - attack.t1071.004
    ...snip

    returns ['T1048.003', 'T1071.004']
    """

    technique_id_list = []
    logging.debug(rule.tags)

    for item in rule.tags:
        tag_parts = str(item).split('.')

        logging.debug(tag_parts)

        if len(tag_parts) == 2 and tag_parts[1][0] == 't':
            technique_id_list.append('T' + tag_parts[1][1:])

        if len(tag_parts) == 3 and tag_parts[1][0] == 't':
            technique_id_list.append('T' + tag_parts[1][1:] + '.' + tag_parts[2])

    return technique_id_list

def sigma_rule_to_elasticsearch_dsl_lucene_query(rule : SigmaRule) -> dict:
    """
        wrapper for convert_rule
    """
    return LuceneBackend().convert_rule(rule, output_format='dsl_lucene')[0]

def convert_sigma_rules(sigma_rule_dir: str) -> dict:
    """
    returns dictionary of converted rules, key = technique id,
    value = list of SigmaToElastic objects
    """
    results = {}

    rules = SigmaCollection.load_ruleset([sigma_rule_dir])
    for rule in rules:
        technique_id_list = extract_technique_ids_from_sigma_rule(rule)
        if len(technique_id_list) > 0:
            try:
                query = sigma_rule_to_elasticsearch_dsl_lucene_query(rule)
            except SigmaConditionError as sigma_error:
                logging.debug(sigma_error)
            else:
                for technique_id in technique_id_list:
                    if technique_id not in results:
                        results[technique_id] = [(SigmaToElastic(rule, query))]
                    else:
                        results[technique_id].append(SigmaToElastic(rule, query))
    return results

# testing
# results = convert_sigma_rules('backend/resources/sigma-rules')
# for k,v in results.items():
#     logging.error(k)
#     for e in v:
#         logging.error(e)
#     logging.error('--')

# rules = SigmaCollection.load_ruleset(['backend/resources/my-own-rules'])
# for r in rules:
#     logging.error(f'rule src: {r.source}')
#     logging.error(convert_sigma_rule_to_elasticsearch_dsl_lucene_query(r))
