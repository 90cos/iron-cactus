"""
Module responsible for running queries against an elasticsearch instance
with the goal of mapping potential indicators of compromise (PIOC) to Mitre
Attack techniques.
"""

# pylint: disable=import-error
# pylint: disable=too-few-public-methods
# pylint: disable=logging-fstring-interpolation

from datetime import datetime
import json
import logging
from os.path import join as path_join

from irca_attack import add_default_attack_layers
from irca_elastic import search_has_results
from irca_layer import create_technique_dict, create_empty_layer, pioc_over_any_layer
from irca_sigma import convert_sigma_rules, SigmaToElastic
from elasticsearch import ElasticsearchException
from opensearchpy.exceptions import RequestError

ASSETS_PATH = 'spa/build/assets/'
EMPTY_ATTACK_LAYER_FILE_REL_PATH = "backend/resources/layer-template.json"

class QueryResult:
    """
    simple wrapper class around the results
    """
    def __init__(self, rule2query: SigmaToElastic, search_hit_count: int, es_doc_id):
        self.rule2query = rule2query
        self.search_hit_count = search_hit_count
        self.es_doc_id = es_doc_id

class TechniqueProcessingResult:
    """
    simple wrapper class around the results
    """
    def __init__(self, technique_id: str):
        self.technique_id = technique_id
        self.identified = False
        self.query_results = [] # list of QueryResult objects

def postprocessed_pioc_data_to_layer_file(data):
    """
    data argument is a list of TechniqueProcessingResult objects
    """
    pioc_generated_layer_file = datetime.now().strftime("%Y-%m-%d_%H-%M-%S_") + \
        "pioc-layer-file.json"

    pioc_layer = {"techniques": []}
    pioc_techniques = pioc_layer["techniques"]

    # item: TechniqueProcessingResult
    for item in data:
        if item.identified:
            new_technique = create_technique_dict(item.technique_id)
            for query_res in item.query_results:
                if query_res.search_hit_count > 0:
                    new_technique["metadata"].extend([
                        {"name": "Query", "value": str(query_res.rule2query.dsl_lucene_query)},
                        {"name": "Hits", "value": str(query_res.search_hit_count)},
                        {"name": "_id", "value": str(query_res.es_doc_id)}
                    ])
            pioc_techniques.append(new_technique)
    main_layer = create_empty_layer()
    main_layer["name"] = pioc_generated_layer_file[:-5]  # remove file extension
    main_layer["filters"]["platforms"] = ['Network', 'Windows', 'Linux']
    pioc_layer = pioc_over_any_layer(pioc_layer, main_layer)

    # serialize our dict to a json file and write to disk
    with open(path_join(ASSETS_PATH, pioc_generated_layer_file), "w",
              encoding="utf-8") as json_fd:
        json.dump(pioc_layer, json_fd, indent=2)

    add_default_attack_layers([f"assets/{pioc_generated_layer_file}"], ASSETS_PATH)
    return True


def process_sigma_rules(elastic_conn, sigma_conversion_results: dict):
    """
    Function iterates through each entry in sigma_conversion_results.
    Returns list of TechniqueProcessingResult objects
    """

    results = []

    for technique_id in sigma_conversion_results.keys():
        tpr = TechniqueProcessingResult(technique_id)

        # each technique has 1 or more queries designed to 'detect' the technique
        # obj is of type: SigmaToElastic
        for obj in sigma_conversion_results[technique_id]:
            try:
                hits, res = search_has_results(elastic_conn, obj.dsl_lucene_query)
            except ElasticsearchException as exc_obj:
                logging.error(f'ElasticsearchException: {exc_obj}')
            except RequestError as exc_obj:
                logging.error(f'OpenSearch RequestError: {exc_obj}')
            else:
                if res:
                    query_result_obj = QueryResult(obj, hits, res)
                    if res is not None:
                        tpr.identified = True
                    tpr.query_results.append(query_result_obj)
                    results.append(tpr)
    return results

def do_pioc(elastic_conn, sigma_rule_dir):
    """Perform the PIOC generation request."""
    sigma_conversion_results_dict = convert_sigma_rules(sigma_rule_dir)
    processing_results = process_sigma_rules(elastic_conn, sigma_conversion_results_dict)

    for item in processing_results:
        if item.identified:
            logging.error(f'technique id: {item.technique_id}')
            logging.error(f'identified: {item.identified}')
            for element in item.query_results:
                logging.error(f'\trule source id: {element.rule2query.src_sigma_rule.source}')
                logging.error(f'\tquery sent: {element.rule2query.dsl_lucene_query}')
                logging.error(f'\thits: {element.search_hit_count}')
                logging.error(f'\tes doc id: {element.es_doc_id}')

    if processing_results is not None:
        return postprocessed_pioc_data_to_layer_file(processing_results)
    return None

# testing
# from elasticsearch import Elasticsearch
# # es = Elasticsearch('http://localhost:9200')
# # r = do_pioc(es, '/home/user/docs/rules')
# # logging.error(r)
