"""
Utility functions for uploading and converting yaml files to json files and/or
uploading json files from a client device to the container
"""
# pylint: disable=import-error
# pylint: disable=logging-fstring-interpolation
# pylint: disable=broad-except

from json import dump as json_dump
from os.path import exists, join as path_join
import logging

from werkzeug.utils import secure_filename
from yaml import safe_load

from irca_attack import add_default_attack_layers
from irca_data_sources import create_data_source_layer
from irca_groups import create_group_layer
from irca_techniques import create_technique_layer


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('IRCA_FILE_UTIL')

JSON_EXTENSIONS = {'json'}
YAML_EXTENSIONS = {'yaml', 'yml'}

UPLOAD_FOLDER = 'spa/build/assets/'

DATA_SOURCE_TYPE = "data-source-administration"
TECHNIQUE_TYPE = "technique-administration"
GROUP_TYPE = "group-administration"


def file_is_ext(file_storage, extensions):
    """Checks if filename is one of the specified extensions."""
    return '.' in file_storage.filename and \
        file_storage.filename.rsplit('.', 1)[1].lower() in extensions


def file_allowed(file_storage):
    """Checks filename extension as a simple check for allowed filetype."""
    return file_is_ext(file_storage, JSON_EXTENSIONS.union(YAML_EXTENSIONS))


def upload_and_convert_dettect_files(request_files, dettect_path,
        local_stix_data):
    """Take a dictionary input of FileStorage files (from Flask request) to
    process and save to corresponding ATT&CK JSON files.

    Assumes all files in request_files are YAML files.
    """
    for file_storage in request_files.values():
        file_content = safe_load(file_storage)
        file_type = file_content["file_type"]
        if file_type == DATA_SOURCE_TYPE:
            layer = create_data_source_layer(file_content, dettect_path,
                                             local_stix_data)
        elif file_type == TECHNIQUE_TYPE:
            layer = create_technique_layer(file_content, dettect_path,
                                           local_stix_data)
        elif file_type == GROUP_TYPE:
            layer = create_group_layer(file_content, dettect_path,
                                       local_stix_data)
        else:
            raise IOError("Unknown DeTT&CT YAML type.")
        layer_name = layer["name"]
        file_name = f"{layer_name}.json"
        file_num = 1
        while True:
            file_path = path_join(UPLOAD_FOLDER, file_name)
            if not exists(file_path):
                break
            file_name = f"{layer_name} ({file_num}).json"
            file_num += 1
        with open(file_path, 'w', encoding="utf-8") as layer_file:
            json_dump(layer, layer_file, indent=2)
        add_default_attack_layers([f"assets/{file_name}"], UPLOAD_FOLDER)


def upload_attack_files(request_files):
    """Takes a Multidict of input json files to upload, and adds them to the
    default layer.

    Assumes all files in request_files are JSON files.
    """
    for file_storage in request_files.values():
        filename = secure_filename(file_storage.filename)
        file_storage.save(path_join(UPLOAD_FOLDER, filename))
        add_default_attack_layers([f"assets/{filename}"], UPLOAD_FOLDER)
