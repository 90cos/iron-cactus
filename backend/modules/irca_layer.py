"""
Contains functions and utilities used to combine ATT&CK navigator layers.

Order of Operations:
1a. Combine all coverage layers using combine_dettect_layers().
1b. Combine all threat layers using combine_dettect_layers().
1c. Combine all PIoC layers using pioc_over_any_layer().
2. Overlay the master threat layer on the master coverage layer using
    threat_over_coverage_layer(master_threat, master_coverage, ...)
3. Overlay the master PIoC layer over your final layer using
    pioc_over_any_layer(master_pioc, master_combined, ...).
"""
# pylint: disable=import-error

import logging
from json import load as json_load, dumps as json_dumps
from stix2 import parse


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("IRCA")

LAYER_TEMPLATE_REL_PATH = "backend/resources/"
# temp add assets/ back to filenames
ENTERPRISE_STIX_FILENAME = "assets/enterprise-attack.json"
MOBILE_STIX_FILENAME = "assets/mobile-attack.json"
ICS_STIX_FILENAME = "assets/ics-attack.json"

STIX_PATH = '/spa/build/'
STIX_ATTACK_TECHNIQUES = [ENTERPRISE_STIX_FILENAME, MOBILE_STIX_FILENAME, ICS_STIX_FILENAME]


def create_technique_dict(technique_id, comment=""):
    """Creates a technique dictionary for use in the
    empty attack layer json file techniques[] array
    """
    return {
        "techniqueID": technique_id,
        "color": "",
        "comment": comment,
        "enabled": True,
        "links": [],
        "metadata": [],
        "showSubtechniques": True
    }

def dict_deep_copy(source):
    """Performs a full copy of a dictionary."""
    dest = source
    if isinstance(source, dict):
        dest = {}
        for key in source:
            dest[key] = dict_deep_copy(source[key])
    elif isinstance(source, list):
        dest = []
        for item in source:
            dest.append(dict_deep_copy(item))
    return dest

def technique_key(technique, use_tactic=False):
    """Returns the techniqueID and optionally the tactic name concatenated.
    The ATT&CK Navigator UI does not allow modifying individual cells of the
    same technique across different tactics.
    """
    return f"{technique['techniqueID']}" \
           f"{technique.get('tactic') if use_tactic else ''}"

def make_technique_dict(layer):
    """Returns a dictionary of all techniques in a layer."""
    return {technique_key(a): a for a in layer["techniques"]}

def create_empty_layer():
    """Rebuilds layer-template.json with all techniques found in the *-attack.json files in
    /spa/assets/ and returns a new blank json template layer."""
    # overwrite layer-template.json with new stix data
    with open(LAYER_TEMPLATE_REL_PATH + "layer-template.json", "r+", encoding='utf-8') as json_fd:
        data = json_load(json_fd)
        # parsed_stix_data should be a list of dicts for techniques : [{...}]
        parsed_stix_data = parse_stix_data()
        data['techniques'] = parsed_stix_data
        json_fd.seek(0)
        json_fd.write(json_dumps(data, indent=4))
    logger.warning('created empty layer with %i techniques', len(parsed_stix_data))
    return data

def parse_stix_data():
    """Parses through STIX files to get and return valid techniques."""
    parsed_techniques = []
    for stix_file in STIX_ATTACK_TECHNIQUES:
        with open(STIX_PATH + stix_file, "r", encoding='utf-8') as json_fd:
            bundle = parse(json_fd, allow_custom=True)
        for obj in bundle.objects:
            if is_valid_technique(obj):
                parsed_techniques.append(create_technique_dict( \
                    obj['external_references'][0]['external_id']))

    # if techniqueID has no decimal, make showSubtechniques: true
    for technique in parsed_techniques:
        technique["enabled"] = False
        if '.' not in technique['techniqueID']:
            technique['showSubtechniques'] = True
        else:
            technique['showSubtechniques'] = False
    return parsed_techniques

def is_valid_technique(json_obj):
    """Checks if STIX technique has valid fields."""
    if 'external_references' in json_obj:
    # and 'kill_chain_phases' in json_obj:
        if json_obj['external_references']:
            if 'external_id' in json_obj['external_references'][0]:
            #  and 'phase_name' in json_obj['kill_chain_phases'][0]:
                return True
    return False



def combine_techniques(technique, add_technique):
    """Combines information of two techniques."""
    technique["color"] = technique.get("color", "") or \
                         add_technique.get("color", "")
    technique["comment"] = f"{add_technique.get('comment', '').strip()}\n\n" \
                           f"{technique.get('comment', '').strip()}".strip()
    technique["enabled"] |= add_technique["enabled"]
    if "metadata" in add_technique:
        metadata = dict_deep_copy(add_technique["metadata"])
        metadata.extend(technique.get("metadata", []))
        technique["metadata"] = metadata
    if "links" in add_technique:
        links = dict_deep_copy(add_technique["links"])
        links.extend(technique.get("links", []))
        technique["links"] = links
    technique["showSubtechniques"] = True

def combine_dettect_layers(additional_layer, main_layer, layer_name=None):
    """Combines two coverage layers or two threat layers into a
    single combined layer.
    """
    result = dict_deep_copy(main_layer)
    if layer_name is not None:
        result["name"] = layer_name
    additional_layer_t = make_technique_dict(additional_layer)
    for technique in result["techniques"]:
        key = technique_key(technique)
        add_technique = additional_layer_t.get(key)
        score = (technique.get("score", 0))
        if add_technique is not None:
            combine_techniques(technique, add_technique)
            if add_technique.get("score") or score:
                technique["score"] = max(add_technique.get("score", 0), score)
        technique["showSubtechniques"] = True
    return result

def threat_over_coverage_layer(threat_layer, coverage_layer, layer_name,
        gradient=None):
    """Create a new layer that overlays threats on a coverage layer."""
    gradient = ["#00000000", "#ffff8888", "#ff8888ff"] \
        if gradient is None else gradient
    result = dict_deep_copy(coverage_layer)
    result["gradient"] = {
        "colors": gradient,
        "minValue": 0,
        "maxValue": 100
    }
    result["name"] = layer_name
    threat_layer_t = make_technique_dict(threat_layer)
    for technique in result["techniques"]:
        key = technique_key(technique)
        threat_technique = threat_layer_t.get(key)
        technique["score"] = technique.get("score", 0) * -1
        if threat_technique is not None:
            combine_techniques(technique, threat_technique)
            technique["score"] += threat_technique.get("score", 0)
        technique["color"] = ""
        technique["score"] = max(technique["score"], 0)
        technique["showSubtechniques"] = True
    return result

def pioc_over_any_layer(pioc_layer, main_layer, layer_name=None,
        highlight_color=None):
    """Create a new layer that overlays PIoCs to a threat layer."""
    highlight_color = "#ff0000ff" \
        if highlight_color is None else highlight_color
    result = dict_deep_copy(main_layer)
    if layer_name is not None:
        result["name"] = layer_name
    pioc_layer_t = make_technique_dict(pioc_layer)
    for technique in result["techniques"]:
        key = technique_key(technique)
        pioc_technique = pioc_layer_t.get(key)
        if pioc_technique is not None and pioc_technique["enabled"]:
            combine_techniques(technique, pioc_technique)
            technique["color"] = highlight_color
    return result
