"""
Flask application
"""
# pylint: disable=import-error
# pylint: disable=global-statement
# pylint: disable=no-member

from datetime import datetime
from json import dump as json_dump, load as json_load, loads as json_loads
import logging
from os.path import join as path_join
from os import remove, system
from shutil import make_archive, unpack_archive

from flask import Flask, request, send_file, send_from_directory, Response

from irca_attack import add_default_attack_layers, get_default_attack_layers, \
                        remove_default_attack_layer, set_stix, reset_stix
from irca_elastic import connect_to_elastic
from irca_file_util import file_allowed, file_is_ext, \
                           upload_and_convert_dettect_files, \
                           upload_attack_files, \
                           JSON_EXTENSIONS, YAML_EXTENSIONS
from irca_groups import create_threat_group_layer, get_threat_groups
from irca_layer import combine_dettect_layers, threat_over_coverage_layer, \
                       pioc_over_any_layer
from irca_pioc import do_pioc


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("IRCA")

ASSETS_PATH = '../../spa/build/assets/'
UPLOAD_FOLDER = ASSETS_PATH
PIOC_CONFIG_DIR = '/rules'
ALLOWED_EXTENSIONS = set(['yml', 'yaml', 'json'])
STIX_PATH = '/local-stix-data/'

APP = Flask(__name__, static_folder='../../spa/build', static_url_path='/')
APP.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# global elastic instance representing connection
G_ELASTIC_CONN = None


@APP.route('/')
def homepage():
    """homepage"""
    return APP.send_static_file('./index.html')


@APP.route('/attack-navigator')
def attacknavigator():
    """attacknavigator"""
    return APP.send_static_file('./attack-navigator.html')


@APP.route('/dettect-editor')
def dettecteditor():
    """dettecteditor"""
    return APP.send_static_file('./dettect-editor.html')


@APP.route('/d3fend')
def d3fend():
    """d3fend"""
    return APP.send_static_file('./d3fend.html')


@APP.route('/connect', methods=['POST'])
def slash_connect():
    """connect to elastic"""
    data = json_loads(request.data)
    hostname = data.get("hostAddress")
    port = int(data.get("portNumber"))
    use_ssl = data.get("useSsl")
    username = data.get("username")
    password = data.get("password")

    global G_ELASTIC_CONN
    # Close existing connection if it exists
    if G_ELASTIC_CONN is not None:
        G_ELASTIC_CONN.close()
    G_ELASTIC_CONN = connect_to_elastic(hostname, port, use_ssl, username,
                                        password)
    if G_ELASTIC_CONN is not None:
        response = {'result': 'success'}, 200
    else:
        response = {'result': 'failure'}, 503
    return response

# invokes pioc and creates a pioc attack layer
@APP.route('/linkage', methods=['POST'])
def slash_pioc():
    """invokes our pioc routine"""
    #logger.warning(f'invoking do_pioc, active pioc file: {ACTIVE_PIOC_CONFIG_DIR}')
    ret = do_pioc(G_ELASTIC_CONN, PIOC_CONFIG_DIR)
    if ret:
        response = {'result': 'success'}, 200
    else:
        response = {'result': 'failure'}, 503
    return response


@APP.route('/newlayer', methods=['POST'])
def newlayer():
    # pylint: disable=unused-variable
    """creates a new layer using threat group data"""
    data = json_loads(request.data)

    # set layer name
    if data.get('layerName') == "":
        layer_name = f"IRCA Threat Groups - {datetime.now()}"
    else:
        layer_name = data.get('layerName')

    threat_groups = data.get('threatGroups')
    response = {'result': 'success'}, 200

    try:
        response = create_threat_group_layer(layer_name, threat_groups, "DeTTECT")
    # pylint: disable=broad-except
    except Exception as error:
        logger.warning(error)
        response = {'result': str(error)}, 500

    return response


@APP.route('/getdefaultlayers', methods=['GET'])
def getdefaultlayers():
    """gets the default layers for attack navigator"""
    return {"layers": list(map(lambda l: l.removeprefix("assets/"),
                               get_default_attack_layers(ASSETS_PATH)))}


@APP.route('/removedefaultlayer', methods=['POST'])
def removedefaultlayer():
    """removes a layer from the default layers list"""
    filename = request.args.get('removefile')
    ret = remove_default_attack_layer(path_join("assets/", filename), ASSETS_PATH)

    if ret is not False:
        response = {'result': 'success'}, 200
    else:
        response = {'result': 'failure'}, 503
    APP.logger.debug(f"remove_default_attack_layer() returned: {ret}")
    return response


@APP.route('/downloadlayers', methods=['GET'])
def downloadlayers():
    """downloads user selected default layers"""
    filepath = ASSETS_PATH
    filename = request.args.get('downloadfile')
    return send_file(path_join(filepath, filename), as_attachment=True,
                     download_name=filename, mimetype='application/json')


@APP.route('/uploadfiles', methods=['POST'])
def uploadfiles():
    """
    Endpoint for uploading yaml and/or json files
    """
    response = {'result': 'failed to upload files'}, 400
    if not request.files:
        response = {'result': 'No files found'}, 400
    elif any(not file_allowed(fs) for fs in request.files.values()):
        response = {'result': 'File type not allowed'}, 400
    else:
        files_json = {k: v for k, v in request.files.items()
                      if file_is_ext(v, JSON_EXTENSIONS)}
        files_yaml = {k: v for k, v in request.files.items()
                      if file_is_ext(v, YAML_EXTENSIONS)}
        try:
            upload_attack_files(files_json)
            upload_and_convert_dettect_files(files_yaml, "DeTTECT",
                                             "local-stix-data")
            response = {'result': 'success'}, 200
        except IOError as ex:
            response = {'result': str(ex)}, 200
    return response


@APP.route('/getthreats', methods=['GET'])
def getthreats():
    """gets the list of valid threat groups for the create threat group
    layer tab"""
    return {"threatGroups": get_threat_groups("DeTTECT", "local-stix-data")}

@APP.route('/getpiocconfig', methods=['GET'])
def get_pioc_config():
    """downloads the currently active pioc dir as an archive"""
    # Zip current /rules
    make_archive('rules', 'zip', PIOC_CONFIG_DIR)

    # Send rules.zip
    with open(path_join('/', 'rules.zip'), 'rb') as fzip:
        data = fzip.readlines()
    remove(path_join('/', 'rules.zip'))
    return Response(data, headers={
        'Content-Type': 'application/zip',
        'Content-Disposition': 'attachment; filename=rules.zip'
    })


@APP.route('/uploadpiocconfig', methods = ['POST'])
def upload_pioc_config():
    """ upload pioc archive"""
    if request.method == 'POST':
        obj = request.files['file']
        if obj.filename.endswith('.zip'):
            # save zip
            obj.save('/user-rules.zip')
            # clear user rules
            system('rm -rf /user-rules')
            # Unzip the Archive to /user-rules
            unpack_archive('/user-rules.zip', '/user-rules', 'zip')
            # remove the zip
            system('rm /user-rules.zip')
            # clear current rules
            system('rm -rf /rules')
            # rename the user rules
            system('mv /user-rules /rules')
        else:
            return {'result': 'bad file extension'}, 400
    return 'file uploaded successfully'


@APP.route('/resetpioc', methods=['POST'])
def resetpioc():
    """renames rules dirs to rest the pioc to default"""
    # clear current rules
    system('rm -rf /rules')
    # rename the default rules
    system('mkdir -p /rules && cp -r /default-rules/* /rules')
    return {"result": "success"}, 200


@APP.route('/combinelayers', methods=['POST'])
def combinelayers():
    """Combines user selected default layers."""
    data = json_loads(request.data)
    layer_one_name = data.get("layerOneName")
    layer_one_type = data.get("layerOneType")
    layer_two_name = data.get("layerTwoName")
    layer_two_type = data.get("layerTwoType")
    layer_name = data.get("layerName", f"IRCA Combined - {datetime.now()}")
    if any(not a for a in (layer_one_name, layer_one_type, layer_two_name,
            layer_two_type)):
        return {'result': "missing field"}, 500

    try:
        with open(path_join(ASSETS_PATH, layer_one_name), "r",
                encoding="utf-8") as fd_one:
            layer_one = json_load(fd_one)
        with open(path_join(ASSETS_PATH, layer_two_name), "r",
                encoding="utf-8") as fd_two:
            layer_two = json_load(fd_two)
        result_layer = None
        if layer_one_type == 3:
            result_layer = pioc_over_any_layer(layer_one, layer_two,
                                               layer_name)
        elif layer_two_type == 3:
            result_layer = pioc_over_any_layer(layer_two, layer_one,
                                               layer_name)
        elif layer_one_type == layer_two_type and layer_one_type in (1, 2):
            result_layer = combine_dettect_layers(layer_two, layer_one,
                                                  layer_name)
        elif layer_one_type == 1 and layer_two_type == 2:
            result_layer = threat_over_coverage_layer(layer_two, layer_one,
                                                      layer_name)
        elif layer_one_type == 2 and layer_two_type == 1:
            result_layer = threat_over_coverage_layer(layer_one, layer_two,
                                                      layer_name)
    # pylint: disable=broad-except
    except Exception as error:
        return {'result': f"{type(error).__name__}: {str(error)}"}, 500

    if result_layer is not None:
        file_name = f"{layer_name}.json"
        with open(path_join(ASSETS_PATH, file_name), 'w', encoding="utf-8") \
                as layer_file:
            json_dump(result_layer, layer_file, indent=2)
        add_default_attack_layers([f"assets/{file_name}"], ASSETS_PATH)
        return {"result": "success"}, 200
    return {'result': "unknown layer type"}, 500


@APP.route('/setstixfile', methods = ['POST'])
def setstixfile():
    """saves uploaded stix data"""
    if request.method == 'POST':
        obj = request.files['file']
        # Check if file is STIX file
        if obj.filename.endswith('.zip'):
            domain = request.args.get('domain')
            # save the zip in the local-stix-data folder
            obj.save(path_join(STIX_PATH, f'{domain}-attack.zip'))
            # set the file name in attack's config file
            if set_stix(ASSETS_PATH, domain):
                return {"result": "success"}, 200
            return {'result': 'Internal Server Error'}, 500
        return {'result': 'bad file extension'}, 400
    return {'result': 'Method Not Allowed'}, 405


@APP.route('/getstixfile', methods=['GET'])
def getstixfile():
    """downloads the currently active STIX files for v11"""
    domain = request.args.get('domain')

    make_archive(f'/local-stix-data/{domain}-attack', 'zip', \
        f'/local-stix-data/{domain}-attack')

    return send_from_directory('/local-stix-data/', f'{domain}-attack.zip', \
        mimetype='application/zip', as_attachment=True, download_name=f'{domain}-attack.zip')


@APP.route('/resetstixfiles', methods=['POST'])
def resetstixfiles():
    """resets the currently active STIX files to the default for v11"""
    reset_stix(ASSETS_PATH)
    return {"result": "success"}, 200


if __name__ == '__main__':
    APP.run(debug=True, host='0.0.0.0', port=5000)
