"""
small module for testing the flask api http endpoints
"""

#!/usr/bin/python3

import requests # pylint: disable=import-error


G_BASE_URL = 'http://127.0.0.1:5000'

# connect
print("[headless irca client] - attempting to connect to elastic")
x = requests.post(G_BASE_URL + '/connect', json = {'hostAddress': 'es01','portNumber': 9200})
print(x.text)


print("[headless irca client] - attempting to invoke pioc endpoint")
if x.status_code == 200:
    x = requests.post(G_BASE_URL + '/pioc')
    print(x.text)
