#!/bin/bash

# General Robot Variables
venv_name=venv

# Setup Virtual Environment
if [ ! -f $venv_name/bin/activate ]; then
    echo Setting up virtual environment at $venv_name...
    python3 -m venv $venv_name
fi

# Activate Virtual Environment and Run Robot
source $venv_name/bin/activate
python3 -m pip install -r ./tests/requirements.txt

mkdir -p ../artifacts
robot -d ../artifacts ./tests/tests.robot

deactivate

rm -rf $venv_name
rm -rf ../artifacts
