#!/bin/bash

# IRCA Notice
# Notice
# This product includes software developed at the 90th Cyberspace Operations Squadron.
# Portions of this computer program were created as a work effort for the United States Government and are not protected by copyright (17 U.S. Code 105). Any person who fraudulently places a copyright notice on, or does any other act contrary to the provisions of 17 U.S. Code 506(c) shall be subject to the penalties provided therein.
# This notice shall not be altered or removed from this software or digital media, and is to be on all reproductions.
# The remaining portions are copyright their respective authors and have been contributed under the terms of one or more open source licenses, and made available to you under the terms of those licenses. (See LICENSE)
# Licensing Intent
# The intent is that this software and documentation ("Project") should be treated as if it is licensed under the license associated with the Project ("License")in the LICENSE file. However, because we are part of the United States (U.S.) Federal Government, it is not that simple.
# The portions of this Project written by U.S. Federal Government employees within the scope of their federal employment are ineligible for copyright protection in the U.S.; this is generally understood to mean that these portions of the Project are placed in the public domain. In countries where copyright protection is available (which does not include the U.S.), contributions made by U.S. Federal Government employees are released under the License.
# Merged contributions from private contributors are released under the License.
# The Project is released under the GNU GENERAL PUBLIC LICENSE V3 (GPL).
# Some modules contain third party components (libraries, icons, etc.) that each have their own license that is compatible with GPL V3. The license files for each license used by the Project can be found adjacent to the third party sources within the source tree.


if [ $# -eq 0 ]
    then
        echo "$(tput bold)$(tput setaf 1)[error] Required file not submitted. Requires an irca tgz file as an argument$(tput sgr0)"
        exit 1
fi
echo "$(tput bold)$(tput setaf 3)[info] unarchiving$(tput sgr0)"
tar -xzf $1

echo "$(tput bold)$(tput setaf 3)[info] loading image into docker$(tput sgr0)"
cd irca
docker load --input irca-docker.tar

cat readme2.txt

echo "$(tput bold)$(tput setaf 3)[info] end of install$(tput sgr0)"
