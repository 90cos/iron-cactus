import React, { Component } from 'react'
import { ThemeProvider, createTheme } from '@mui/material/styles'
import Bar from './components/Bar'
import TabBox from './components/TabBox'
import './App.css'


const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

class App extends Component {
  state = {
    fields: {}
  };

  onChange = updatedValue => {
    this.setState({
      fields: {
        ...this.state.fields,
        ...updatedValue
      }
    });
  };

  render() {
    return (
      <ThemeProvider theme={darkTheme}>
        <Bar />
        <div className="App">
          <div className="main">
            <TabBox/>
          </div>
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
