import React, { useEffect, useState }  from 'react'
import Alert from '@mui/material/Alert'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import CloseIcon from '@mui/icons-material/Close'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import FormControl from '@mui/material/FormControl'
import HelpIcon from '@mui/icons-material/Help'
import IconButton from '@mui/material/IconButton'
import MenuItem from '@mui/material/MenuItem'
import MuiButton from '@mui/material/Button'
import PropTypes from 'prop-types'
import Select from '@mui/material/Select'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export function CombinatorHelp() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon/>
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Combinator Layer Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Required fields: Layer Name, 2 layers along with type of layer.
          </Typography>
          <Typography gutterBottom>
            Valid Combinations: PIOC to PIOC, Coverage to Coverage, Threat to Threat, Coverage to Threat
          </Typography>
          <Typography gutterBottom>
            To generate a combination you need to make a PIOC layer, APT layer, and Coverage layer. Two or more layers needed to use the Combinator.
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  );
}

export const Button = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "46px",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '76%'
}));

export function Combinator() {
  const [layers, setLayers] = useState([])
  const [layerName, setLayerName] = useState('')
  const [layerOneName, setLayerOneName] = useState('')
  const [layerTwoName, setLayerTwoName] = useState('')
  const [layerOneType, setLayerOneType] = useState(1)
  const [layerTwoType, setLayerTwoType] = useState(1)
  const [goodPopup, setGoodPopup] = useState(false)
  const [badPopup, setBadPopup] = useState(false)
  const [popupWords, setPopupWords] = useState('')
  const [wait, setWait] = useState(false)

  useEffect(() => {
    fetch(window.location.href + 'getdefaultlayers')
      .then((res) => res.json())
      .then((json) => {
          setLayers(json["layers"])
    })
  }, [])

  const onSubmit = e => {
    e.preventDefault();
    setBadPopup(false)
    setPopupWords('Combining layer...!')
    setGoodPopup(true)
    setWait(true)

    let state = {
      layerOneName: layerOneName,
      layerOneType: layerOneType,
      layerTwoName: layerTwoName,
      layerTwoType: layerTwoType,
      layerName: layerName,
    }

    //Options for API request
    let options = {
      method: 'POST',
      body: JSON.stringify(state),
    };

    fetch(window.location.href + `combinelayers`, options)
      .then(res => {
        try{
          if(res.status === 200) {
            setBadPopup(false)
            setPopupWords('Layer Combined!')
            setGoodPopup(true)
            setWait(false)
          }else{
            setBadPopup(true)
            setPopupWords('Layer Combining Failed!')
            setGoodPopup(false)
            setWait(false)
          }}catch(error) {
          setBadPopup(true)
          setPopupWords('Layer Combining Failed!')
          setGoodPopup(false)
          setWait(false)
      }})
  }

  return (
    <>
     <Box sx={{ width: '100%' }}>
        <Collapse in={goodPopup}>
          <Alert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setGoodPopup(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            {popupWords}
          </Alert>
        </Collapse>
        <Collapse in={badPopup}>
          <Alert severity="error"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setBadPopup(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            {popupWords}
          </Alert>
        </Collapse>
      </Box>
      <CombinatorHelp/>
      <div>
      {(layers && layers.length <= 1) ? <p className='noData'>Two or more layers needed to use the Combinator.</p> : (
      <form onSubmit={e => onSubmit(e)} className="form">
        <input
          className="combinatorName"
          name="layerName"
          placeholder="Layer Name"
          required
          value={layerName}
          onChange={e => setLayerName(e.target.value)}
        />
        <Box
          sx={{
            width: '91%',
          }}
        >
          <FormControl
            sx={{
              width: '45%',
              marginLeft: 6,
              background: "#474747",
              cursor: "pointer",
              borderRadius: "5em",
              border: "0",
            }}
          >
            <Select
              sx={{
                background: "#474747",
                cursor: "pointer",
                borderRadius: "5em",
                border: "0",
                fontFamily: "Ubuntu, sans-serif",
              }}
              defaultValue=''
              name="layerOneName"
              labelId="layerOneNameLabel"
              id="layerOneName"
              value={layerOneName}
              label="Layer One"
              onChange={e => setLayerOneName(e.target.value)}
              required
              displayEmpty
            >
              <MenuItem value=''>
                Select First Layer
              </MenuItem>
              {layers.map(layer => (
                <MenuItem key={layer} value={layer}>{layer}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl sx={{
            width: '30%',
            marginLeft: 6,
            background: "#474747",
            cursor: "pointer",
            borderRadius: "5em",
            border: "0",}}
          >
            <Select
              sx={{
                background: "#474747",
                cursor: "pointer",
                borderRadius: "5em",
                border: "0",
                fontFamily: "Ubuntu, sans-serif",
              }}
              defaultValue={1}
              labelId="layerOneTypeLabel"
              id="layerOneType"
              name="layerOneType"
              value={layerOneType}
              label="Layer Type"
              onChange={e => setLayerOneType(e.target.value)}
              required
            >
              <MenuItem value={1}>Coverage</MenuItem>
              <MenuItem value={2}>APT</MenuItem>
              <MenuItem value={3}>PIOC</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <br/>
        <Box
          sx={{
            width: '91%',
          }}
        >
          <FormControl sx={{
            width: '45%',
            marginLeft: 6,
            background: "#474747",
            cursor: "pointer",
            borderRadius: "5em",
            border: "0",}}
          >
            <Select
              sx={{
                background: "#474747",
                cursor: "pointer",
                borderRadius: "5em",
                border: "0",
                fontFamily: "Ubuntu, sans-serif",
              }}
              defaultValue=''
              labelId="layerTwoNameLabel"
              id="layerTwoName"
              name="layerTwoName"
              value={layerTwoName}
              label="Layer Two"
              onChange={e => setLayerTwoName(e.target.value)}
              required
              displayEmpty
            >
              <MenuItem value=''>
                Select Second Layer
              </MenuItem>
                {layers.map(layer => (
                <MenuItem key={layer} value={layer}>{layer}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl sx={{
            width: '30%',
            marginLeft: 6,
            background: "#474747",
            cursor: "pointer",
            borderRadius: "5em",
            border: "0",}}
          >
            <Select
              sx={{
                background: "#474747",
                cursor: "pointer",
                borderRadius: "5em",
                border: "0",
                fontFamily: "Ubuntu, sans-serif",
              }}
              defaultValue={1}
              labelId="layerTwoTypeLabel"
              id="layerTwoType"
              name="layerTwoType"
              value={layerTwoType}
              label="Layer Type"
              onChange={e => setLayerTwoType(e.target.value)}
              required
            >
              <MenuItem value={1}>Coverage</MenuItem>
              <MenuItem value={2}>APT</MenuItem>
              <MenuItem value={3}>PIOC</MenuItem>
            </Select>
          </FormControl>
          </Box>
          <br/>
          <br/>
          <br/>
          <Button
            variant="contained"
            type="submit"
            className="submitFile"
            disabled={wait}
            onClick={(e) => onSubmit(e)}
            id="submitLayer"
            >
              Combine
          </Button>
        </form>
        )}
        </div>
    </>
    );
}

export default class Connect extends React.Component {

  render() {
    return (
      <>
        <Combinator/>
      </>
    );
  }
}