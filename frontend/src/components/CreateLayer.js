import React, { useState, useEffect } from 'react'
import Alert from '@mui/material/Alert'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import CloseIcon from '@mui/icons-material/Close'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import HelpIcon from '@mui/icons-material/Help'
import IconButton from '@mui/material/IconButton'
import MuiButton from '@mui/material/Button'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'


export const Button = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "7%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '76%'        
}))

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export function CreateHelp() {
  const [open, setOpen] = React.useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon />
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Create Layer Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Enter a layer name and select one or more threat groups to create a 
            new layer in ATT&CK Navigator.
          </Typography>
          <Typography gutterBottom>
            If not name is entered, it will create with a default layer name.
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  )
}

export function CreateLayer() {
  const [layerName, setLayerName] = useState('')
  const [checkedItems, setCheckedItems] = useState({})
  const [threats, setThreats] = useState([])
  const [goodPopup, setGoodPopup] = useState(false)
  const [badPopup, setBadPopup] = useState(false)
  const [popupWords, setPopupWords] = useState('')
  const [wait, setWait] = useState(false)


  useEffect(() => {
    fetch(window.location.href + 'getthreats')
      .then((res) => res.json())
      .then((json) => {
        setThreats(json["threatGroups"])
      })
  }, [])

  const handleChange = event => {
    setCheckedItems({...checkedItems, [event.target.name] : event.target.checked });
  }

  const onSubmit = e => {
    e.preventDefault()
    setBadPopup(false)
    setPopupWords('Creating layer...')
    setGoodPopup(true)
    setWait(true)

    let state = {
      layerName: layerName,
      threatGroups: checkedItems
    }

    //Options for API request
    let options = {
      headers: {
        Accept: "application/json",    
        "Content-Type": "application/json",
      },
      method: 'POST',
      mode: 'no-cors', 
      body: JSON.stringify(state),
    }
    fetch(window.location.href + `newlayer`, options)
      .then(res => {
        try{
          if(res.status === 200) {
            setGoodPopup(true)
            setBadPopup(false)
            setPopupWords('Layer Created!')
            setWait(false)
          }else if(res.status === 503){
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Layer Creation Failed! Invalid threat group or no techniques found for threat group.')
            setWait(false)
          }else{
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Layer Creation Failed!')
            setWait(false)
          }
        }
        catch(error) {
          setGoodPopup(false)
          setBadPopup(true)
          setPopupWords('Layer Creation Failed!')
          setWait(false)
      }})
  }
    return (
      <>
        <Box sx={{ width: '100%' }}>
          <Collapse in={goodPopup}>
            <Alert
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setGoodPopup(false);
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              sx={{ mb: 2 }}
            >
              {popupWords}
            </Alert>
          </Collapse>
          <Collapse in={badPopup}>
            <Alert severity="error"
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setBadPopup(false);
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              sx={{ mb: 2 }}
            >
              {popupWords}
            </Alert>
          </Collapse>
        </Box>
        <CreateHelp/>
        <form onSubmit={e => onSubmit(e)} className="form">
          <input
            className="layerName"
            name="layerName"
            placeholder="Layer Name"
            value={layerName}
            onChange={e => setLayerName(e.target.value)}
          />
          <div className="results" >
            <table>
              {threats.map(threat => (
                <tr key={threat.name}>
                  <td>{threat.name}</td>
                  <td>
                    <input
                      type="checkbox"
                      name={threat.name}
                      checked={checkedItems[threat.name]} 
                      onChange={e => handleChange(e)}
                    />
                  </td>
                </tr>
              ))}
            </table>
          </div>          
          <br/>
          <Button
            variant="contained"
            type="submit"
            className="submit"
            disabled={wait}
            onClick={(e) => onSubmit(e)}
            id="submitLayer"
          >
            Create
          </Button>
        </form>
      </>
    )
  }

export default class Connect extends React.Component {

  render() {
    return (
      <>
        <CreateLayer/>
      </>
    )
  }
}