import React, { useState }  from 'react'
import Box from '@mui/material/Box'
import Alert from '@mui/material/Alert'
import Collapse from '@mui/material/Collapse'
import CloseIcon from '@mui/icons-material/Close'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import HelpIcon from '@mui/icons-material/Help'
import IconButton from '@mui/material/IconButton'
import MuiButton from '@mui/material/Button'
import MuISelect from '@mui/material/Select'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import RestartAltIcon from '@mui/icons-material/RestartAlt'


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export function STIXHelp() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon/>
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          STIX Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
          This tab allows you to download the current domain STIX files in use by Mitre ATT&CK Navigator. Domain STIX files in use can be reset to the default STIX files.
          </Typography>
          <Typography gutterBottom>
          This tab also allows you to upload STIX files to be used Mitre ATT&CK Navigator v11 for Enterprise, Mobile, and ICS domains.
          STIX data should be the root of a .zip, in the same format that is found in the domain files at https://github.com/mitre/cti or the default STIX data
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  );
}

export const Select = styled(MuISelect)(() => ({
  background: "#474747",
  cursor: "pointer",
  borderRadius: "5em",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  '&:hover': {
    color: 'white',
    backgroundColor: '#42A5F5',
  },
}));

export const Button = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "12%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '76%'
}));

export const ResetButton = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "65%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '30%',
  backgroundColor: 'red',
}));

export function Stixi() {
  const [stixType, setStixType] = useState('')
  const [goodPopup, setGoodPopup] = useState(false)
  const [badPopup, setBadPopup] = useState(false)
  const [popupWords, setPopupWords] = useState('')
  const [wait, setWait] = useState(false)

  const handleChange = (event) => {
    setStixType(event.target.value);
  };

  const onSubmit = e => {
    e.preventDefault()
    setBadPopup(false)
    setPopupWords('Uploading File, this may take a while...')
    setGoodPopup(true)
    setWait(true)

    let data = new FormData()
    data.append('file', e.target.files[0]);

    //Options for API request
    let options = {
      method: 'POST',
      body: data,
    };

    fetch(window.location.href + `setstixfile?domain=` + e.target.name, options )
      .then(res => {
        try{
          if(res.status === 200) {
            setGoodPopup(true)
            setBadPopup(false)
            setPopupWords('Uploaded File! Close ATT&CK for changes to take affect.')
            setWait(false)
          }else if(res.status === 404){
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Uploading File Failed! Could not unarchive STIX data.')
            setWait(false)
          }else{
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Uploading Files Failed!')
            setWait(false)
          }
        }catch(error) {
          setGoodPopup(false)
          setBadPopup(true)
          setPopupWords('Uploading Files Failed!')
          setWait(false)
      }})
      e.target.value=null
  }

  const onDownload = e => {
    e.preventDefault();
    setBadPopup(false)
    setWait(true)

    fetch(window.location.href + 'getstixfile?domain=' + e.target.name)
      .then(res => {
        try{
          if(res.status === 200) {
            window.open(window.location.href + 'getstixfile?domain=' + e.target.name, '_blank');
            setGoodPopup(true)
            setBadPopup(false)
            setPopupWords('Download Successful!')
            setWait(false)
          }else{
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Download Failed!')
            setWait(false)}
          }catch(error) {
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Download Failed!')
            setWait(false)
      }})
  }

  const onReset= e => {
    e.preventDefault();
    setBadPopup(false)
    setWait(true)

    //Options for API request
    let options = {
        method: 'POST',
    };

    fetch(window.location.href + 'resetstixfiles', options)
      .then(res => {
        try{
          if(res.status === 200) {
              setGoodPopup(true)
              setBadPopup(false)
              setPopupWords('STIX Files Reset! Close ATT&CK for changes to take affect.')
              setWait(false)
            }else{
              setGoodPopup(false)
              setBadPopup(true)
              setPopupWords('STIX Reset Failed!')
              setWait(false)
            }
          }catch(error) {
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('STIX Reset Failed!')
            setWait(false)
      }})
  }

  return (
    <>
      <Box sx={{ width: '100%' }}>
        <Collapse in={goodPopup}>
          <Alert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setGoodPopup(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            {popupWords}
          </Alert>
        </Collapse>
        <Collapse in={badPopup}>
          <Alert severity="error"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setBadPopup(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            {popupWords}
          </Alert>
        </Collapse>
      </Box>
      <STIXHelp/>
      <FormControl variant="filled"
        sx={{
          minWidth: '76%',
          marginLeft: "12%",
          textAlign: 'center',
          background: "#474747",
          borderRadius: "5em",
          border: "0",
        }}>
        <InputLabel id="stix-dropdown" disableAnimation="true"
          sx={{
            marginLeft: '10',
            textTransform: "None",
            textAlign: 'center',
          }}
        >STIX Data</InputLabel>
        <Select
          disableUnderline='true'
          labelId="stix-dropdown-label"
          id="stix-dropdown-select"
          value={stixType}
          label="STIX"
          onChange={handleChange}
        >
          <MenuItem value={'enterprise'}>Enterprise STIX</MenuItem>
          <MenuItem value={'mobile'}>Mobile STIX</MenuItem>
          <MenuItem value={'ics'}>ICS STIX</MenuItem>
        </Select>
      </FormControl>
      <br/><br/>
      <Button
        variant="contained"
        component="label"
        textColor="inherit"
        name={stixType}
        onChange={e => onSubmit(e)}
        disabled={wait || !stixType}
      >
        Upload STIX File
        <input
          accept='.zip'
          multiple type="file"
          name={stixType}
          autoComplete='false'
          hidden
        />
      </Button>
      <br/><br/>
      <Button
        variant="contained"
        value="Download"
        name={stixType}
        onClick={(e) => onDownload(e)}
        disabled={wait || !stixType}
        >
          Download STIX File
      </Button>
      <br/><br/>
      <br/><br/>
      <ResetButton
        startIcon={<RestartAltIcon />}
        variant="contained"
        value="Download"
        onClick={(e) => onReset(e)}
        disabled={wait}
        >
          Reset STIX Files to Defualt
      </ResetButton>
    </>
  );
}

export default class Connect extends React.Component {

  render() {
    return (
      <>
        <Stixi/>
      </>
    );
  }
}