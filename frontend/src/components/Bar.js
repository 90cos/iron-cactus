import React, {createContext, useState} from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import CloseIcon from '@mui/icons-material/Close'
import Collapse from '@mui/material/Collapse'
import Dialog from '@mui/material/Dialog'
import Divider from '@mui/material/Divider'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import IconButton from '@mui/material/IconButton'
import Link from '@mui/material/Link'
import List from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import ReactMarkdown from 'react-markdown'
import Slide from '@mui/material/Slide'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import '../App.css'


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const ThemeContext = createContext(null);

export default function DenseAppBar() {
  const [theme, setTheme] = useState("dark");
  const [open, setOpen] = React.useState(false)
  const [openDett, setOpenDett] = React.useState(true);
  const [openAtt, setOpenAtt] = React.useState(true);
  const [openDef, setOpenDef] = React.useState(true);
  const [openInst, setOpenInst] = React.useState(true);

  const handleInst = () => {
    setOpenInst(!openInst);
  };

  const handleDett = () => {
    setOpenDett(!openDett);
  };

  const handleAtt = () => {
    setOpenAtt(!openAtt);
  };

  const handleDef = () => {
    setOpenDef(!openDef);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const toggleTheme = () => {
    setTheme((curr) => (curr === "light" ? "dark" : "light"));
  };

  const instMd =
  '#### -----------Naviagate to localhost:port to reach each tool----------- \n \n' +
  'IRCA                    - localhost:5000\n \n' +
  'Dettect                 - localhost:5000/dettect-editor\n \n' +
  'Attack                  - localhost:5000/attack-navigator\n \n' +
  'Defend                  - localhost:5000/d3fend\n \n' +
  '#### -----------IRCA Tab Information----------\n \n' +
  '#### Connect:\n \n' +
  'User input: Host Address, Port Number, and Enable PIOC Box\n' +
  'Entering just the Host Address and Port Number will test your Elastic connection.\n' +
  'Enabling PIOC will launch the process for finding Potential Indicators of Compromise (PIOC).\n \n' +
  '#### Threat Group:\n \n' +
  'Enter a layer name and select one or more threat groups to create a new layer in ATT&CK Navigator.\n' +
  'If not name is entered, it will create with a default layer name.\n \n' +
  '#### Upload:\n \n' +
  'Allowed extensions: .json, .yaml, and .yml\n' +
  'Multiple file uploads on single multiple selection of files.\n \n' +
  '#### Layer Management:\n \n' +
  'Default layers that will populate Attack Navigator. Click "download" to download the layer and click "remove" to delete a layer from Attack Navigator.\n' +
  'Download: Downloads selected layer.\n' +
  'Remove: Deletes selected layer.\n \n' +
  '#### Combinator:\n \n' +
  'Required fields: Layer Name, 2 layers along with type of layer.\n' +
  'Valid Combinations: PIOC to PIOC, Coverage to Coverage, Threat to Threat, Coverage to Threat\n' +
  'To generate a combination you need to make a PIOC layer, APT layer, and Coverage layer. At least one layer needs to appear to see information on the Combination page.\n\n'+
  '#### PIOC:\n \n' +
  'PIOC or Potential Indicators of Compromise, is a way to query ElasticSearch based on sigma rules (https://github.com/SigmaHQ/sigma).\n' +
  'This tab allows you to download the current sigma rules being used as well as upload custom sigma rules you wish to run against ElasticSearch.\n' +
  'For example sigma rules, download the current PIOC config or go to https://github.com/SigmaHQ/sigma for more examples and information on sigma rules.\n \n' +
  '#### STIX:\n \n' +
  'Download the current domain STIX files in use by Mitre ATT&CK Navigator. Domain STIX files in use can be reset to the default STIX files.\n' +
  'Allows you to upload STIX files to be used Mitre ATT&CK Navigator v11 for Enterprise, Mobile, and ICS domains.\n' +
  'STIX data should be the root of a .zip, in the same format that is found in the domain files at https://github.com/mitre/cti or the default STIX data\n'

  const detMd = '#### Detect Tactics, Techniques & Combat Threats\n ' +
  'Latest version: [1.6.0](https://github.com/rabobank-cdc/DeTTECT/wiki/Changelog#version-160)\n' +
  '\nTo get started with DeTT&CT, check out one of these resources:\n' +
  '- This [page](https://github.com/rabobank-cdc/DeTTECT/wiki/Getting-started) on the Wiki.\n' +
  '- This [blog](https://blog.nviso.eu/2022/03/09/dettct-mapping-detection-to-mitre-attck/) written by [Renaud Frère](https://twitter.com/Azotium) from NVISO has a comprehensive and recent description on the capabilities of DeTT&CT.\n' +
  '- Blog: [mbsecure.nl/blog/2019/5/dettact-mapping-your-blue-team-to-mitre-attack](https://www.mbsecure.nl/blog/2019/5/dettact-mapping-your-blue-team-to-mitre-attack) or\n' +
  '- Blog: [siriussecurity.nl/blog/2019/5/8/mapping-your-blue-team-to-mitre-attack](https://www.siriussecurity.nl/blog/2019/5/8/mapping-your-blue-team-to-mitre-attack).\n  \n' +
  '**Videos**\n  \n' +
  '- DeTT&CT [talk](https://www.youtube.com/watch?v=_kWpekkhomU) at hack.lu 2019.\n' +
  '- The [video](https://www.youtube.com/watch?v=EXnutTLKS5o) from [Justin Henderson](https://twitter.com/SecurityMapper) on data source visibility and mapping.\n' +
  'DeTT&CT aims to assist blue teams in using ATT&CK to score and compare data log source quality, visibility coverage, detection coverage and threat actor behaviours. All of which can help, in different ways, to get more resilient against attacks targeting your organisation. The DeTT&CT framework consists of a Python tool (DeTT&CT CLI), YAML administration files, the [DeTT&CT Editor](https://rabobank-cdc.github.io/dettect-editor) (to create and edit the YAML administration files) and [scoring tables](https://github.com/rabobank-cdc/DeTTECT/raw/master/scoring_table.xlsx) for [detections](https://github.com/rabobank-cdc/DeTTECT/wiki/How-to-use-the-framework#detection), [data sources](https://github.com/rabobank-cdc/DeTTECT/wiki/How-to-use-the-framework#data-source) and [visibility](https://github.com/rabobank-cdc/DeTTECT/wiki/How-to-use-the-framework#visibility).\n  \n' +
  'DeTT&CT provides the following functionality for the ATT&CK domains Enterprise and ICS:\n  \n' +
  '- Administrate and score the quality of your data sources.\n' +
  '- Get insight on the visibility you have on for example endpoints.\n' +
  '- Map your detection coverage.\n' +
  '- Map threat actor behaviours.\n' +
  '- Compare visibility, detection coverage and threat actor behaviours to uncover possible improvements in detection and visibility (which is based on your available data sources). This can help you to prioritise your blue teaming efforts.\n' +
  '- Get statistics (per platform) on the number of techniques covered per data source.\n  \n' +
  'The coloured visualisations are created with the help of MITREs [ATT&CK™ Navigator](https://mitre-attack.github.io/attack-navigator/#comment_underline=false). *For layer files created by DeTT&CT, it is recommended to use this URL for the Navigator as it will make sure metadata in the layer file does not have a yellow underline: [https://mitre-attack.github.io/attack-navigator/#comment_underline=false](https://mitre-attack.github.io/attack-navigator/#comment_underline=false)*\n  \n' +
  '## Authors and contributions\n  \n' +
  'This project is developed and maintained by [Marcus Bakker](https://github.com/marcusbakker) (Twitter: [@Bakk3rM](https://twitter.com/Bakk3rM)) and [Ruben Bouman](https://github.com/rubinatorz) (Twitter: [@rubinatorz](https://twitter.com/rubinatorz/)). Feel free to contact, DMs are open. Ask questions on how to use DeTT&CT by making a GitHub issue. Having the questions and answers over there will greatly help others having similar questions and challenges.\n  \n' +
  'Contributions are welcome! Contributions can be both in code and in ideas you might have for further development, usability improvements, etc.\n  \n' +
  '### Sponsors\n  \n' +
  'The following parties have supported the development of DeTT&CT in time or financially.\n  \n' +
  '- **[Rabobank](https://www.rabobank.com/en/home/index.html)** - *Dutch multinational banking and financial services company. Food and agribusiness constitute the primary international focus of the Rabobank.*\n  \n' +
  '\n' +
  '- **[Cyber Security Sharing & Analytics (CSSA)](https://cssa.de/en/index.html#top)** - *Founded in November 2014 by seven major German companies as an alliance for jointly facing cyber security challenges in a proactive, fast and effective manner. Currently, CSSA has 13 member companies.*\n  \n' +
  'Support for [ATT&CK ICS](https://collaborate.mitre.org/attackics/index.php/Main_Page) added to DeTT&CT.\n  \n' +
  '### Work of others\n  \n' +
  'The work of others inspired some functionality within DeTT&CT:\n  \n' +
  '- Roberto Rodriguezs work on data quality and scoring of MITRE ATT&CK™ techniques ([How Hot Is Your Hunt Team?](https://cyberwardog.blogspot.com/2017/07/how-hot-is-your-hunt-team.html), [Ready to hunt? First, Show me your data!](https://cyberwardog.blogspot.com/2017/12/ready-to-hunt-first-show-me-your-data.html)).\n' +
  '- The MITRE ATT&CK Mapping project on GitHub: https://github.com/siriussecurity/mitre-attack-mapping.\n  \n' +
  '## Example\n  \n' +
  'YAML files are used for administrating scores and relevant properties. All of which can be visualised by loading JSON layer files into the [ATT&CK Navigator](https://mitre-attack.github.io/attack-navigator/#comment_underline=false) (some types of scores and properties can also be exported to Excel).\n  \n' +
  'Using the command `python dettect.py generic -ds`, you can determine which data sources within ATT&CK cover the most techniques. This can, for example, be useful to guide you in identifying which data sources will provide you with a lot of visibility and are hence a good candidate to have available in a SIEM (like) solution.\n  \n' +
  'Count  Data Source\n  \n' +
  '--------------------------------------------------\n  \n' +
  '255    Command Execution\n  \n' +
  '206    Process Creation\n  \n' +
  '98     File Modification\n  \n' +
  '88     File Creation\n  \n' +
  '82     Network Traffic Flow\n  \n' +
  '78     OS API Execution\n  \n' +
  '70     Network Traffic Content\n  \n' +
  '58     Windows Registry Key Modification\n  \n' +
  '58     Network Connection Creation\n  \n' +
  '55     Application Log Content\n  \n' +
  '50     Module Load\n  \n' +
  '46     File Access\n  \n' +
  '46     Web [DeTT&CT data source]\n  \n' +
  '37     File Metadata\n  \n' +
  '32     Logon Session Creation\n  \n' +
  '26     Script Execution\n  \n' +
  '22     Response Content\n  \n' +
  '21     Internal DNS [DeTT&CT data source]\n  \n' +
  '20     User Account Authentication\n  \n' +
  '18     Process Access\n  \n' +
  '17     Windows Registry Key Creation\n  \n' +
  '17     Email [DeTT&CT data source]\n  \n' +
  '15     Service Creation\n  \n' +
  '15     Host Status\n  \n' +
  '13     Active Directory Object Modification\n  \n' +
  '12     Service Metadata\n  \n' +
  '11     Process Metadata\n  \n' +
  '10     Driver Load\n  \n' +
  '10     File Deletion\n  \n' +
  '9      Firmware Modification\n  \n' +
  '9      Logon Session Metadata\n  \n' +
  '9      Process Modification\n  \n' +
  '8      User Account Metadata\n  \n' +
  '7      Windows Registry Key Access\n  \n' +
  '7      Scheduled Job Creation\n  \n' +
  '7      Malware Metadata\n  \n' +
  '7      Active Directory Credential Request\n  \n' +
  '6      Container Creation\n  \n' +
  '6      Web Credential Usage\n  \n' +
  '6      Response Metadata\n  \n' +
  '6      User Account Creation\n  \n' +
  '6      Drive Modification\n  \n' +
  '6      User Account Modification\n  \n' +
  '5      Instance Creation\n  \n' +
  '5      Active DNS\n  \n' +
  '5      Passive DNS\n  \n' +
  '5      Network Share Access\n  \n' +
  '5      Drive Access\n  \n' +
  '5      Service Modification\n  \n' +
  '4      Image Creation\n  \n' +
  '4      Instance Start\n  \n' +
  '4      Active Directory Object Creation\n  \n' +
  '4      Malware Content\n  \n' +
  '4      Social Media\n  \n' +
  '4      Domain Registration\n  \n' +
  '4      Drive Creation\n  \n' +
  '4      Windows Registry Key Deletion\n  \n' +
  '3      Active Directory Object Access\n  \n' +
  '3      Instance Metadata\n  \n' +
  '3      Container Start\n  \n' +
  '3      Web Credential Creation\n  \n' +
  '3      Firewall Rule Modification\n  \n' +
  '3      Firewall Disable\n  \n' +
  '3      Instance Deletion\n  \n' +
  '3      Snapshot Creation\n  \n' +
  '3      Process Termination\n  \n' +
  '2      Cloud Storage Enumeration\n  \n' +
  '2      Cloud Storage Access\n  \n' +
  '2      Pod Metadata\n  \n' +
  '2      Active Directory Object Deletion\n  \n' +
  '2      Cloud Service Modification\n  \n' +
  '2      Cloud Service Disable\n  \n' +
  '2      Certificate Registration\n  \n' +
  '2      Cloud Storage Metadata\n  \n' +
  '2      Instance Modification\n  \n' +
  '2      Instance Stop\n  \n' +
  '2      Firewall Metadata\n  \n' +
  '2      Firewall Enumeration\n  \n' +
  '2      Group Enumeration\n  \n' +
  '2      Group Metadata\n  \n' +
  '2      Image Metadata\n  \n' +
  '2      Scheduled Job Metadata\n  \n' +
  '2      Scheduled Job Modification\n  \n' +
  '2      Kernel Module Load\n  \n' +
  '2      WMI Creation\n  \n' +
  '2      Group Modification\n  \n' +
  '2      Driver Metadata\n  \n' +
  '2      Snapshot Modification\n  \n' +
  '2      Snapshot Deletion\n  \n' +
  '2      Volume Deletion\n  \n' +
  '2      Cloud Storage Modification\n  \n' +
  '2      Cloud Service Enumeration\n  \n' +
  '1      Cluster Metadata\n  \n' +
  '1      Container Enumeration\n  \n' +
  '1      Container Metadata\n  \n' +
  '1      Pod Enumeration\n  \n' +
  '1      Pod Creation\n  \n' +
  '1      Pod Modification\n  \n' +
  '1      Instance Enumeration\n  \n' +
  '1      Snapshot Metadata\n  \n' +
  '1      Snapshot Enumeration\n  \n' +
  '1      Volume Metadata\n  \n' +
  '1      Volume Enumeration\n  \n' +
  '1      Named Pipe Metadata\n  \n' +
  '1      User Account Deletion\n  \n' +
  '1      Image Modificationv\n  \n' +
  '1      Volume Creation\n  \n' +
  '1      Volume Modification\n  \n' +
  '1      Cloud Storage Creation\n  \n' +
  '1      Cloud Service Metadata\n  \n' +
  '1      Image Deletion\n  \n' +
  '1      Cloud Storage Deletion\n  \n' +
  '1      DHCP [DeTT&CT data source]\n  \n' +
  '## License: GPL-3.0\n  \n' +
  '[DeTT&CTs GNU General Public License v3.0](https://github.com/rabobank-cdc/DeTTECT/blob/master/LICENSE)\n  \n'

  const attMd = 'The ATT&CK Navigator is designed to provide basic navigation and annotation of ATT&CK matrices, something that people are already doing today in tools like Excel. It is designed it to be simple and generic - you can use the Navigator to visualize your defensive coverage, your red/blue team planning, the frequency of detected techniques or anything else you want to do. The Navigator does not care - it just allows you to manipulate the cells in the matrix (color coding, adding a comment, assigning a numerical value, etc.).\n  \n' +
  'The principal feature of the Navigator is the ability for users to define layers - custom views of the ATT&CK knowledge base - e.g. showing just those techniques for a particular platform or highlighting techniques a specific adversary has been known to use. Layers can be created interactively within the Navigator or generated programmatically and then visualized via the Navigator.\n  \n' +
  'Bug reports and feature requests can be submitted to [GitHub Issue Tracker](https://github.com/mitre-attack/attack-navigator/issues). The source code for the ATT&CK Navigator can be retrieved from our [GitHub repository](https://github.com/mitre-attack/attack-navigator).\n  \n' +
  '## Layers\n  \n' +
  'A layer constitutes a view of the tactics and techniques matrix for a specific technology domain. In more recent versions, the Navigator can manipulate either the Enterprise, Mobile, or ICS ATT&CK technology domain knowledge bases. Within a technology domain, the Navigator allows you to filter your view of the matrix in a variety of ways, displaying the tactics and techniques that are important to you.\n  \n' +
  'You can view the definition of any technique in the visible matrix by right-clicking on the technique and selecting "view technique" in the pop-up menu. A new browser tab will be opened displaying the definition of the technique. In this way the Navigator allows you to explore a given ATT&CK matrix and access the definitions of the techniques.\n  \n' +
  'Beyond the filters, layers also provide a means to customize your view of the matrix. To that end, you can color, hide, comment, and assign, numeric scores, links, metadata to techniques to aid in analysis of threats and your defenses against those threats. As stated earlier, the Navigator is designed to be simple, allowing you to assign whatever meaning you want to the color-codings, scores, and comments. This way the Navigator can support whatever you want to do without requiring changes to the Navigator code itself.\n  \n' +
  'Each layer created is independent of other layers. However, layers can be combined in ways to support analysis, or saved locally. Layer files are saved in easy to parse and easy to generate JSON file so that ATT&CK data can be used in other applications, analyzed beyond the capability of the ATT&CK Navigator, and generated by tools for import into the Navigator.\n  \n' +
  '## Creating New Layers\n  \n' +
  'To create a new layer, open a new tab and click the "Create New Layer" dropdown. The quick access buttons will create a layer with the current version of ATT&CK. Past versions of ATT&CK compatible with the Navigator are accessible in the "More Options" dropdown. This interface allows you to specify the desired version and domain for the new layer. Note that for performance reasons, the Navigator currently imposes a limit of ten (10) active layers at any given point in time.\n  \n' +
  '## Saving and Loading Layers\n  \n' +
  'Layers can be saved by clicking the "save layer" button. This will open a dialog to save a layer configuration file to your local computer. This contains the configuration for the techniques that have been customized (commented, colored, disabled, or assigned a score, links, or metadata) as well as the scoring gradient setup, filter selection, layer name, layer description, view configuration.\n  \n' +
  'Saved layer configuration files can be opened in the ATT&CK Navigator to restore a layer you have worked on previously. To do so, open a new tab and open the "Open Existing Layer" panel. Then click "Upload from local", and select your saved configuration file. Doing so will restore your saved layer to the ATT&CK Navigator. This interface also has a "load from URL" input allowing you to open a layer json from a remote source.\n  \n' +
  '## Upgrading a Layer to the Current Version\n  \n' +
  'The layer upgrade interface allows users to upgrade an ATT&CK Navigator layer created on a previous version of ATT&CK to the current version of the dataset. With this interface, users can step through each change in the dataset of techniques:\n  \n' +
  '- The user can view techniques which have been added to the dataset since the layer was created and annotate them as necessary.\n  \n' +
  '- The user can see whether the techniques they had previously annotated have changed and adjust the annotations accordingly.\n  \n' +
  '- The user can determine if any annotated techniques have been removed or replaced by new techniques and in the latter case copy annotations to the replacing technique(s).\n  \n' +
  '- The user can also verify what techniques have not changed since the layer was created.\n  \n' +
  '### Mapping Annotations\n  \n' +
  'For annotated techniques which have changed since the layer was created, you may want to update the set of annotations to account for changes to scope or content. Each technique in the interface lists its previous and current state, with links to [the ATT&CK Website](https://attack.mitre.org/) for both versions of the technique to enable easy review. In steps with techniques you have previously annotated, you can enable the "show annotated techniques only" filter to view only those techniques which have annotations.\n  \n' +
  'You can copy annotations from the previous version of the technique to the current one, making adjustments if necessary. There are two approaches to copying annotations from the previous version:\n  \n' +
  '- For techniques with tactics that map one-to-one between the two versions, annotations can be copied by clicking the ">" button next to the tactic. This will copy any existing annotations on the tactic to the technique under the same tactic in the current version.\n  \n' +
  '- For techniques with tactics that have changed between the two versions, annotations can be copied by dragging & dropping the annotated tactic from the previous version to the tactic(s) in the current version.\n  \n' +
  'Techniques can be marked as reviewed under their respective panels to visually indicate that they have been reviewed. The "reviewed X/Y techniques" displayed underneath the techniques list can be used to determine if you missed any techniques. The numerator reflects the number of techniques you have marked as "reviewed" and the denominator denotes the total number of techniques shown according to your configuration (for example, if you have enabled the "show annotated techniques only" filter, only the number of techniques which are annotated are counted in this total).\n  \n' +
  'You can view and verify the status of the layer upgrade at the end of the workflow to ensure you have created or adjusted all annotations as desired. A status is displayed next to each section name to indicate either the number of techniques you have reviewed in the section, if you have skipped the section, or if there are no techniques to review in the section. Once you have finished upgrading the layer, you can click the "done" button to close the sidebar.\n  \n' +
  'Note: You will not be able to return to the layer upgrade interface after the sidebar is closed.\n  \n' +
  '## Creating Layers from Other Layers\n  \n' +
  'Layers can be created which inherit properties from other layers. Several fields exist which can be used to choose which layers to inherit properties from:\n  \n' +
  '- **Domain:** choose the domain and version for the new layer. Layers can only inherit properties from other layers of the same domain and version.\n  \n' +
  '- **Score Expression:** Technique scores in the created layer are initialized to the result of this expression. This field should take the form of an equation or constant, using variables for layers.Layer variables are shown in yellow on tabs when the "create layer from other layers" panel is opened.\n  \n' +
  'Each technique score is created independently using the score expression. For example, with a score expression of a+b, some technique t in the output layer would be the sum of a and t b.\n  \n' +
  'Expressions can also be comparative, for example a>b will create a layer with a score of 1 whereever b and 0 whereever a<=b. Such boolean expressions can be extended using and, or, xor and not. You can also use ternary expressions such as a > b ? 25 : 10. See [this page](http://mathjs.org/docs/expressions/syntax.html#operators) for a full list of operators.\n  \n' +
  'Some example score expressions:\n  \n' +
  '- `(a+b)/2` (average two layers)\n  \n' +
  '- `a+b+c`\n  \n' +
  '- `a*(b+c)`\n  \n' +
  '- `100-a` (reverse the scoring of `a`, assuming `a` is on a 0-100 scale)\n  \n' +
  '- `(a>b) and (a>=75)`\n  \n' +
  '- **Gradient:** choose which layer to inherit #scoring-gradient scoring gradient from.\n  \n' +
  '- **Coloring:** choose which layer to inherit #assigning-manual-colors manually assigned technique colors.\n  \n' +
  '- **Comments:** choose which layer to inherit #adding-comments-to-techniques technique comments from.\n  \n' +
  '- **Links:** choose which layer to inherit #assigning-links-to-technique" assigned links from\n  \n' +
  '- **Metadata:** choose which layer to inherit #adding-metadata-to-techniques technique metadata from.\n  \n' +
  '- **States:** choose which layer to inherit technique #disabling-techniques enabled/disabled states from.\n  \n' +
  '- **Filters:** choose which layer to inherit #filtering layer filter configuration from.\n  \n' +
  '- **Legend:** choose which layer to inherit #legend-bar legend items from.\n  \n' +
  'Tactic-spanning Techniques are evaluated individually: if a technique is annotated differently in two tactics, the output layers techniques will honor this difference.\n  \n' +
  '**Tip:** Score expressions do not need to use variables! You can use this to create a new layer with a constant score for each technique. For example, if you wanted a new layer where all techniques are scored 50, you could simply type 50 into the score expression input.\n  \n' +
  '# Layer Controls\n  \n' +
  '## ![Layer Information](nav-app/src/assets/icons/ic_description_black_24px.svg)Layer Information\n  \n' +
  'The layer name and description can be edited in the layer information dropdown. The layer name can also be edited where it appears in the tab title. Additionally, the layer information panel allows the user to add metadata and assign links to the layer. Metadata can be useful for supporting other applications that use the layer format, or for attaching additional descriptive fields to the layer. Links can be useful for providing additional context from other websites that are relevant to the layer.\n  \n' +
  '## Sorting\n  \n' +
  'There are four modes of sorting. Clicking the sorting button will toggle between the modes.\n  \n' +
  '- The table will sort techniques alphabetically by name in ascending order.\n  \n' +
  '- The table will sort techniques alphabetically by name in descending order.\n  \n' +
  '- The table will sort techniques by their score in ascending order. Techniques with no score are treated as if their score is 0.\n  \n' +
  '- The table will sort techniques by their score in descending order. Techniques with no score are treated as if their score is 0.\n  \n' +
  '## Filtering\n  \n' +
  'The list of techniques and tactics can be filtered in the filtering menu. Filters are additive - the displayed set of techniques is the logical *or* of the techniques of the filters selected.\n  \n' +
  '### Platform Filter\n  \n' +
  'The platform filter allows the user to control which techniques are included in a layer based on whether a particular technique applies to a particular technology platform. Technology platforms are tied to the specific technology domain you are visualizing. For the Enterprise technology domain, the defined platforms are:PRE, Windows, Linux, macOS, Network, AWS, GCP, Azure, Azure AD, Office 365, and SaaS. For the Mobile technology domain, the defined platforms are: Android and iOS. For the ICS technology domain, the defined platforms are Windows, Control Server, Data Historian, "Engineering Workstation", Field Controller/RTU/PLC/IED, Human-Machine Interface, Input/Output Server, Safety Instrumented System/Protection Relay.\n  \n' +
  'Each technique in an ATT&CK matrix is tied to one or more platforms. In the Navigator, if you wanted to see only those techniques in Enterprise ATT&CK which applied to the Linux platform, you would deselect "Windows" and "macOS" under the platform filter. If later you decided to also include techniques known to apply to macOS platforms, you could select "macOS" as well and those techniques would be added to the visible layer.\n  \n' +
  '**Tip:** Techniques can also be hidden from your view by using the hide disabled techniques button. Couple this with the multiselect interface to hide techniques which are contained in specific threat or software groupings.\n  \n' +
  '## Color Setup\n  \n' +
  '### Tactic Row Background\n  \n' +
  'The background color of the tactic row can be set in the tactic row background section of the color setup menu. The color will only be displayed if the "show" checkbox is selected. The tactic row background will not be shown when in the mini view.\n  \n' +
  '### Scoring Gradient\n  \n' +
  'Techniques which are assigned a score will be colored according to a gradient defined in the scoring gradient section in the color setup menu. Technique scores are mapped to a color scaled linearly between the "low value" and "high value" inputs. For example, on a red-green scale, if "low value" were set to 0 and "high value" were set to 50, a score of 25 would fall on yellow -- exactly halfway between red and green. Scores below the low value are colored as if they have the low value, and scores above the high value are colored as if they have the high value.\n  \n' +
  'Several preset gradients are present within the preset dropdown. If no preset matches your desired gradient, you can create your own by adding and removing colors using the interface.\n  \n' +
  '**Tip:** If your scores are binary (0 or 1), consider setting the low value of 0 to transparent and the high of 1 to some other color to only color the techniques which have the value of 1.\n  \n' +
  '## Hiding Disabled Techniques\n  \n' +
  'Techniques that are disabled can be hidden by toggling the "hide disabled techniques" button. Hidden techniques are still present in the data when saved and can still be annotated, but will not be visible in the view.\n  \n' +
  '**Tip:** This button has powerful synergy with the multiselect interface. Use the multiselect interface to select techniques which match your criteria,disable them, and then turn on hiding disabled techniques to remove entire groups of techniques from your view.\n  \n' +
  '## Showing or Hiding Sub-techniques\n  \n' +
  'Sub-techniques in the view are nested under their parent technique and are hidden by default.\n  \n' +
  '- All sub-techniques can be shown by clicking the "expand sub-techniques" button.\n  \n' +
  '- All annotated sub-techniques can be shown by clicking the "expand annotated sub-techniques" button.\n  \n' +
  '- All sub-techniques can be hidden by clicking the "collapse sub-techniques" button.\n  \n' +
  '**Tip:** Sub-techniques can also be shown or hidden on each individual technique when in the side layout or flat layou by clicking the gray sidebar on the technique cell.\n  \n' +
  '## Configuring the Layout\n  \n' +
  'The ATT&CK Navigator has controls for how the ATT&CK Matrices are displayed. Access controls to change layout via the "Matrix Configuration" dropdown menu.\n  \n' +
  '### Side Layout\n  \n' +
  'The side layout displays sub-techniques adjacent to their parent techniques. Techniques with sub-techniques are denoted by the presence of a right-positioned sidebar which can be clicked to show sub-techniques. Sub-techniques are differentiated from techniques by position in the tactic column.\n  \n' +
  '### Flat Layout\n  \n' +
  'The flat layout displays subtechniques in-line with the techniques. Techniques with subtechniques are denoted by the presence of a left-positioned sidebar which can be clicked to show subtechniques. Subtechniques are differentiated from techniques by indentation.\n  \n' +
  '### Mini Layout\n  \n' +
  'The mini layout is designed to fit more techniques on the screen simultaneously by reducing their size. To do so all text is removed and techniques are visualized as squares under the tactic. Selecting this layout disables the "show IDs" and "show Names" controls. Tactic headers are visualized as black cells above the columns. Technique and tactic names are displayed as tooltips when you hover over a technique or tactic-header cell. Techniques and their sub-techniques are grouped inside of an outlined box. The technique is the dark-outlined first cell of the group, and the rest of the cells of the group are the sub-techniques. Techniques without sub-techniques are displayed without a grouping box, and may appear inline with other sub-techniques-less techniques. Disabled techniques are denoted with an "x" symbol. Techniques with comments are denoted with an "i" symbol.\n  \n' +
  '### Showing IDs and Names\n  \n' +
  'In the side and flat layouts, you can change what is shown inside of the technique cells. Enabling "show names" (enabled by default) will show technique and tactic names on each cell of the matrix. Enabling "show IDs" (disabled by default) will show ATT&CK IDs (e.g "T1000" for techniques, or "TA1000" for tactics) on each cell of the matrix. These controls can be toggled independently and turned off entirely to remove cell labels entirely. The mini layout forces both of these controls to be disabled.\n  \n' +
  '## Aggregate Scores\n  \n' +
  'Aggregate scores will combine the scores of a technique and all of its sub-techniques, and can be calculated using either the average, max, min or sum function. They will display only on techniques with sub-techniques. The display of aggregate scores in the matrix view and the score calculations can be customized via the "Matrix Configuration" dropdown menu.\n  \n' +
  '### Showing or Hiding Aggregate Scores\n  \n' +
  'Aggregate scores in the view are displayed in the tooltip when hovering over a technique, and are hidden by default. When this is enabled, the techniques background color will be calculated using the aggregate score by default. \n  \n' +
  '### Counting Unscored Techniques\n  \n' +
  'By default, techniques which are unscored are not included in aggregate score computations. Enabling "count unscored techniques as 0" will make unscored techniques count as if their scores were 0 when computing an aggregate score.\n  \n' +
  '### Choosing an Aggregate Function\n  \n' +
  'There are 4 available functions to calculate the aggregate score: average, min, max, and sum.\n  \n' +
  '- Average: if including unscored techniques, will add up the scores of a technique and all of its sub-techniques, and divide by all; if not including unscored techniques, will add up the onlythe nonzero scores of a technique and its sub-techniques, and divide by the count of non-zeroscores.\n  \n' +
  '- Min: will take the minimum of the scores of a technique and all of its sub-techniques.\n  \n' +
  '- Max: will take the maximum of the scores of a technique and all of its sub-techniques.\n  \n' +
  '- Sum: will add the scores of a technique and all of its sub-techniques.\n  \n' +
  '## Legend Bar\n  \n' +
  'The legend helps associate meanings with colors displayed by customized techniques in the ATT&CK Navigator. To open the legend, click on the bar labeled "legend" in the bottom-right corner of the screen. Click on the same bar to close the legend. To add an item to the legend, click the "Add Item"button. To clear all items in the legend, click "Clear". \n  \n' +
  'An items color can be changed by either clicking in the color field and typing a hex color value, or by clicking in the field and choosing a color from the color picker. Click and type in the text field to change the items label. To remove an item, click on the button on the right side. Legend items are saved to the layer file and will be loaded when a layer with saved legend items is loaded.\n  \n' +
  '# Technique Controls\n  \n' +
  'Techniques in the layer can be annotated. The technique controls on the menubar are only enabled when one or more techniques are selected. If multiple techniques are selected, they will all be annotated simultaneously.\n  \n' +
  '## Disabling Techniques\n  \n' +
  'Clicking the "toggle state" button toggles selected techniques between an enabled and disabled state. In the disabled state, the technique text is greyed out and no colors assigned manually or via will be displayed.\n  \n' +
  'The hide disabled techniques button can be used to hide disabled techniques from the view.\n  \n' +
  '## Assigning Manual Colors\n  \n' +
  'Techniques can be assigned colors manually. Manually assigned colors supersede colors created by score. To remove a manually assigned color, select the "no color"box at the top of the interface.\n  \n' +
  '## Scoring Techniques\n  \n' +
  'A score is a numeric value assigned to a technique. The meaning or interpretation of scores is completely up to the user user - the Navigator simply visualizes the matrix based on any scores you have assigned. Some possible uses of scores include:\n  \n' +
  '- Assigning a score to techniques based on whether a given adversary group has been observed to use that technique.\n  \n' +
  '- Assigning a score to techniques based on your organizations ability to detect, prevent and/or mitigate the use of a particular technique.\n  \n' +
  '- Assigning a score to those techniques that a red-team has successfully employed during an exercise.\n  \n' +
  'By default, techniques are "unscored" meaning that no score has been assigned to the technique. Note that "unscored" and a score of zero are not the same, specifically with respect to automatically assigned colors. Scores show up in technique tooltips if a score has been assigned. To change a technique with a numeric score to unscored, select the technique and delete the score value in the score control. The technique will revert to unscored.\n  \n' +
  'Techniques are automatically assigned a color according to its score. This color is determined according to the scoring gradient setup interface. Colors assigned manually supersede the score-generated color. It is a good idea to assign techniques scores inside of a predetermined range, such as 0-1 or 0-100. Set the "high value" and "low value" inputs in the scoring gradient setup interface to this range to make sure that the color for the score is properly mapped to the gradient. Techniques that are unscored are not assigned a color based on the gradient - they are displayed with an uncolored background in the matrix.\n  \n' +
  '## Adding Comments to Techniques\n  \n' +
  'A text comment can be added to techniques. This comment will show up in the technique tooltip if a comment has been added. Techniques with a comment will be given a yellow underline.\n  \n' +
  '**Note:** A yellow underline is also shown if metadata has been added to the technique or if the technique has attached Notes in the source data. Notes cannot be edited in the Navigator and are displayed in the tooltip.\n  \n' +
  '## Assigning Links to Techniques\n  \n' +
  'Links can be assigned to techniques by specifying a label and a URL for each link. URLs must be prefixed with a protocol identifier. Multiple links can be added by clicking "add links" in the interface. These are displayed in the context menu (accessed by right clicking on a technique) and will open a new browser tab when clicked. To visually separate the links in the context menu, a divider can be added in the interface which will display a horizontal line in the context menu where the divider occurs in the list of assigned links. Techniques with assigned links will be given a blue underline.\n  \n' +
  '**Note**: Links can only be added, updated, or removed if the list of links of all the currently selected techniques are identical, including dividers.\n  \n' +
  '## Adding Metadata to Techniques\n  \n' +
  'Technique metadata can be added by specifying metadata names and values and are displayed in the technique tooltip. Metadata is useful for adding supplemental descriptive fields and information to techniques. To visually separate metadata fields, a divider can be added in the interface, which will display a horizontal line in the tooltip where the divider occurs in the list of metadata. Techniques with metadata will be given a yellow underline.\n  \n' +
  '**Note**: Metadata can only be added, updated, or removed if the list of metadata of all the currently selected techniques are identical, including dividers.\n  \n' +
  '## Clearing Annotations on Techniques\n  \n' +
  'Clicking the "clear annotations on selected" button removes comments, links, metadata, colors, scores, and enabled/disabled state from all selected techniques.\n  \n' +
  '# Selecting Techniques\n  \n' +
  'In order to be annotated, techniques must first be selected. There are multiple ways to select techniques.\n  \n' +
  '## Selecting with the Mouse\n  \n' +
  'Techniques can be selected using the mouse. Left click a technique to select it. Pressing control (windows) command (mac) or shift (both) while left-clicking a technique will add it to or remove it from the selection. Right clicking a technique will bring up a context menu with more options:\n  \n' +
  '- **select:** Select only this technique.\n  \n' +
  '- **add to selection:** Add this technique to the selection.\n  \n' +
  '- **remove from selection:** Remove this technique from the selection.\n  \n' +
  '- **invert selection:** Select all techniques that are not currently selected and unselect all techniques that are currently selected.\n  \n' +
  '- **select all:** Select all techniques.\n  \n' +
  '- **deselect all:** Deselect all techniques. This action can also be completed by the "deselect" button.\n  \n' +
  '- **select annotated:** Select all techniques and sub-techniques which have annotations or remove unnanotated techniques from an existing selection.\n  \n' +
  '- **select unannotated:** Select all techniques and sub-techniques which do not have annotations or remove annotated techniques from an existing selection.\n  \n' +
  '- **select all techniques in tactic:** Select all techniques in this tactic. This action can also be completed by clicking on the tactic header and follows the behavior preference under selection behavior in the selection controls.If the “select techniques across tactics” control is enabled, the selection will include instances of techniques that are in other tactics. Disable this control to select only the instances of techniques that are within this tactic. Sub-techniques within the tactic can be selected along with their parent technique by enabling the “select sub-techniques with parent” control.\n  \n' +
  '- **deselect all techniques in tactic:** Deselect all techniques in this tactic. This action follows the behavior preference under selection behavior in the selection controls.If the “select techniques across tactics” control is enabled, instances of techniques across all tactics will be deselected. Disable this control to remove only the instances of techniques within this tactic from the selection. Sub-techniques in this tactic will be deselected with their parent technique if the “select sub-techniques with parent” control is enabled. Note that currently selected sub-techniques will remain selected if the control is disabled when using this option. \n  \n' +
  '- **view technique:** For more information / details on the technique.\n  \n' +
  '- **view tactic:** For more information / details on the tactic.\n  \n' +
  '- **user assigned links:** List of links assigned to the technique by the user. These links will open a new browser tab directed to the specified URL. See Assigning Links to Techniques for more details.\n  \n' +
  '**Tip:** You can use "select unannotated" followed by disabling those techniques, and then hiding disabled techniques,to create a layer where only annotated techniques are visible.\n  \n' +
  '## Selection Behavior\n  \n' +
  'The selection behavior controls affect how sub-techniques are selected with regards to tactics and sub-techniques.\n  \n' +
  '- **Select techniques across tactics** toggles whether selecting a technique that is found in multiple tactics selects it in all tactics, or just the one wherein it was clicked.\n  \n' +
  '- **Select sub-techniques with parent** toggles whether sub-techniques are selected alongside their parents, and vice versa. When enabled, clicking on a technique will also select all of the sub-techniques of that technique. In addition,clicking a sub-technique will also select the parent as well as all sibling sub-techniques. When disabled, sub-techniques are selected independently of parents and siblings.\n  \n' +
  '## Search & Multiselect Interface\n  \n' +
  'The search & multiselect interface provides the means to select or deselect techniques in the matrix that match a text query or that are mapped to groups, software, or mitigations. The text input can be used to filter the lists of techniques, groups, software, and mitigations according to their data. You can select what fields of the objects are searched under "search settings:" *name*, *ATT&CK ID*, *description*, and (for techniques) *data sources* can all be searched.\n  \n' +
  'The lists of objects below the search can be used to select data in the matrix.\n  \n' +
  '- **Techniques:** this list can be used to find a technique alphabetically or one that matches the search query.\n  \n' +
  '- **Threat Groups:** threat groups constitute related intrusion activity tracked under a common name. Selecting a group under this section will select all techniques that are mapped to (used by) that group.\n  \n' +
  '- **Software:** software constitutes malware (custom closed source or open source software intended for malicious purposes)or tools (open-source, built-in, or publicly available software that could be used by a defender or an adversary). Selecting a software under this section will select all techniques that are mapped to (used by) that software.\n  \n' +
  '- **Mitigations:** Mitigations represent security concepts and classes of technologies that can be used to prevent a technique or sub-technique from being successfully executed. Selecting a mitigation under this section will select all techniques that are mapped to (mitigated by) that mitigation.\n  \n' +
  'The interface provides buttons to select and deselect each object. These buttons modify the currently selected techniques rather than replacing then, allowing for the selection of the multiple techniques or the techniques of multiple threat groups, software, or mitigations by selecting them in sequence. The view links for each entry link to additional information about the object in question.\n  \n' +
  'Buttons labelled select all and deselect all are provided to quickly select/deselect all techniques in the results area. You can use this in conjunction with the search input to select all results which match the given query.\n  \n' +
  '# Customizing the Navigator\n  \n' +
  'The ATT&CK Navigator can be customized by modifying the fragment (e.g `example.com**#fragment**`) of the URL. A panel on the new tab page exists to build a properly formatted ATT&CK Navigator URL such that, when opened, it will create an ATT&CK Navigator instance with the desired customizations. This feature may be useful for sharing or embedding the ATT&CK Navigator.\n  \n' +
  '## Default Layers\n  \n' +
  'Click the "add a layer link" button, then enter a default layer URL pointing to a layer hosted on the web. This will cause the customized ATT&CK Navigator to initialize with this layer open by default. This is especially useful for embedding or sharing specific layers.\n  \n' +
  'You can click the "add another layer link" button to specify additional default layers, or click the "x" button next to a layer link you have already added to remove it.\n  \n' +
  'The following is an example ATT&CK Navigator URL with the default layer specified to be the *Bear APTs layer from [MITRE ATTACK](https://github.com/mitre-attack/attack-navigator) the github repo: [MITRE ATTACK Enterprise](https://mitre-attack.github.io/attack-navigator/enterprise/)\n  \n' +
  'Users will not be prompted to upgrade default layers to the current version of ATT&CK if they are outdated.\n  \n' +
  '## Disabling Features\n  \n' +
  'Individual ATT&CK Navigator features can be disabled with the checkboxes. Removing a feature only removes the interface elements of the feature -- opened layers utilizing those features will still have them present. For example, even if comments are disabled layers with comments present will still display them visually and in tooltips.\n  \n' +
  'If you are hosting your own navigator instance, you can also disable features by editing the configuration file `assets/config.json`.\n  \n' +
  'The following is an example ATT&CK Navigator URL with the ability to download the layer and add comments disabled: [MITRE ATTACK Enterprise](https://mitre-attack.github.io/attack-navigator/enterprise/) **#download_layer=false&comments=false**`\n  \n' +
  '# Rendering Layers as SVG\n  \n' +
  'Clicking the "render layer to SVG" button will open a pop-up window allowing the current layer to be rendered to an SVG image. Clicking the *download svg* button will download the image, as displayed, in SVG format.\n  \n' +
  '**Note:** this feature has minor compatibility warnings with the Internet Explorer browser. For best results, please use Firefox, Chrome or Edge.\n  \n' +
  'The Microsoft Edge browser has a bug where the downloaded SVG will have the `.txt` extension. Renaming the extension to `.svg` will restore it as a valid svg file.\n  \n' +
  '## Measurement Units\n  \n' +
  'Clicking the "toggle measurement unit" button will toggle between measuring in inches (in), centimeters (cm), and pixels (px). This unit applies to controls for image size and legend position.\n  \n' +
  '## Configuring Image Size\n  \n' +
  'The image size controls allow you to specify the width and height of the image, as well as the height of the header if one is present. The measurements are in units specified by the measurement units control.\n  \n' +
  'The header height contributes to the total image height: if you have specified the image height to be 8.5 inches and the header height to be 1 inch, the technique table will be 7.5 inches and the header 1 inch for a total height of 8.5 inches. If the header is disabled this control will not be editable.\n  \n' +
  '## Configuring Text\n  \n' +
  'The text configuration dropdown allows for the configuration of the font (serif, sans-serif, and monospace) of the exported render.\n  \n' +
  'Unlike in previous versions of the Navigator, in more recent versions of the ATT&CK Navigator text size is automatically calculated to optimize readability.\n  \n' +
  '## Customizing the Legend\n  \n' +
  'This menu can only be opened if a legend is present on the layer or if techniques have been assigned scores. The checkbox allows you to undock the legend from the SVG header. Once undocked, the X and Y position controls can be used to position the legend in the image. The width and height inputs control the size of the legend when it is undocked. The measurements are in units specified by the measurement units control.\n  \n' +
  '## Display Settings\n  \n' +
  'The header itself, or specific parts of the header, can be hidden using the controls in this dropdown. The color of table cell borders can also be edited.\n  \n' +
  '- **Show header** controls whether the header is shown at all. If the legend is undocked from the header it will still be shown.\n  \n' +
  '- **Show about** controls whether the about (layer name and description) section of the header is visible. If the layer has no name nor description, the control will be disabled and the section automatically hidden.\n  \n' +
  '- **Show domain** controls whether the domain (layer domain and version) section is visible in the header. This control will be disabled if the header is hidden in entirety.\n  \n' +
  '- **Show filters** controls whether the current filters (selected platforms) are visible in the header. This control will be disabled if the header is hidden in entirety.\n  \n' +
  '- **Show legend** controls whether the layer legend is visible. If the layer has no defined legend items or no scores, the control will be disabled and the legend will be automatically hidden.\n  \n' +
  '- The **sub-techniques** dropdown controls the visibility of sub-techniques in the rendered layer. Selecting "show all" will show all sub-techniques, "show expanded" will show sub-techniques whose parent techniques were expanded when the render button was clicked, and "show none" will show no sub-techniques.\n  \n' +
  '- **Cell border** controls the border of cells in the body of the table. The borders in the header are not modified by this control. Note that you must click "apply" in the color picker for your change to take effect.\n  \n' +
  '# Exporting Layers to MS Excel\n  \n' +
  'Layers can be exported to MS excel (.xlsx) format. Clicking on the "export to excel" button in the toolbar will download an .xlsx file which contains the current layer. This layer contains the annotations from the view -- color (via score or manually assigned) and disabled states. The exporter also honors tactic header background, sorting, filtering and hidden techniques.\n  \n' +
  '# Notice\n  \n' +
  'Copyright 2020 The MITRE Corporation\n  \n' +
  'Approved for Public Release; Distribution Unlimited. Case Number 18-0128.\n  \n' +
  'Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at\n  \n' +
  '[Apache Licenses](http://www.apache.org/licenses/LICENSE-2.0)\n  \n' +
  'Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.\n  \n' +
  'This project makes use of ATT&CK&reg;\n  \n' +
  '[ATT&CK&reg; Terms of Use](https://attack.mitre.org/resources/terms-of-use/)\n  \n'



  const defMd = 'Please visit https://d3fend.mitre.org\n  \n'  +
  '## NOTICE\n  \n' +
  'Use of the MITRE D3FEND™ Knowledge Graph and website is subject to the Terms of Use. Use of the MITRE D3FEND website is subject to the MITRE D3FEND Privacy Policy. MITRE D3FEND is funded by the National Security Agency (NSA) Cybersecurity Directorate and managed by the National Security Engineering Center (NSEC), which is operated by The MITRE Corporation. MITRE D3FEND; and the MITRE D3FEND logo are trademarks of The MITRE Corporation. MITRE ATT&CK® and ATT&CK® are registered trademarks of The MITRE Corporation. MITRE ATT&CK content is subject to the MITRE ATT&CK terms of use.\n  \n' +
  '## NOTICE\n  \n' +
  'This software was produced for the U. S. Government under Basic Contract No. W56KGU-18-D-0004, and is subject to the Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation Clause 252.227-7014 (FEB 2012) © 2022 The MITRE Corporation.\n  \n' +
  '## Frequently Asked Questions\n  \n' +
  '## What is D3FEND?\n  \n' +
  'D3FEND is a knowledge base, but more specifically a knowledge graph, of cybersecurity countermeasure techniques. In the simplest sense, it is a catalog of defensive cybersecurity techniques and their relationships to offensive/adversary techniques. The primary goal of the initial D3FEND release is to help standardize the vocabulary used to describe defensive cybersecurity technology functionality.\n  \n' +
  '## What is D3FEND not?\n  \n' +
  'D3FEND does not prescribe specific countermeasures, it does not prioritize them, and it does not characterize their effectiveness. However, standardizing the vocabulary used to describe technical countermeasures may help us solve those problems.\n  \n' +
  '## Who is D3FEND for?\n  \n' +
  'D3FEND has multiple audiences. The most immediate is security systems architecture experts and technical executives making acquisition or investment decisions. If you need to understand how cyber defenses work in granular detail, D3FEND is meant to be a good starting point.\n  \n' +
  '## What are D3FEND use cases?\n  \n' +
  'The dominant use case thus far has been to inform acquisition and investment. It can do this in two ways.\n  \n' +
  'First, it can be used to compare the claimed functionality in multiple product solution sets with a common defensive technique taxonomy. This makes it possible to identify product differences and product gaps relative to desired functionality in a more precise, consistent, and repeatable manner.\n  \n' +
  'Second, it can suggest a potential testing scope for the defensive techniques in terms of relevant offensive techniques. This is done by identifying a product or product sets claimed defensive techniques, then querying D3FEND for the potentially related offensive techniques. An offensive test plan can be constructed by selecting combinations of the related offensive techniques. This sort of testing can be useful to determine how well a defensive product performs its claimed functionality.\n  \n' +
  '## What is the maturity level of D3FEND?\n  \n' +
  'D3FEND is at an early stage and is an experimental research project. The initial release is not considered comprehensive, and the defensive to offensive technique mappings (which are inferentially generated) are fundamentally generalizations. However, expert cybersecurity knowledge is often, at its essence, the application of fundamental computer system knowledge.\n  \n' +
  '## How often is D3FEND updated?\n  \n' +
  'The target update frequency will eventually be semi-annually. Though as of 2021, updates will be more frequent since the project is new.\n  \n' +
  '## What does a listed reference in D3FEND mean or imply?\n  \n' +
  'D3FEND references a number of different types of internet links. For example, patents, external knowledgebases, open specficiations, and even open source code repositories. The references are used for the purposes of developing the generic countermeasure technique knowledgebase article.\n  \n' +
  '## What does D3FEND stand for?\n  \n' +
  'Detection, Denial, and Disruption Framework Empowering Network Defense.\n  \n'

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Iron Cactus
            </Typography>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <Link><a href="d3fend" target="_blank">D3FEND</a></Link>
            </Typography>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <Link><a href="attack-navigator#comment_underline=false" target="_blank">ATT&CK</a></Link>
            </Typography>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <Link><a href="dettect-editor" target="_blank">DeTT&CT</a></Link>
            </Typography>
            <Button color="inherit" onClick={handleClickOpen}>Readme</Button>
            <Dialog
              fullScreen
              open={open}
              onClose={handleClose}
              TransitionComponent={Transition}
            >
              <AppBar sx={{ position: 'relative' }}>
                <Toolbar>
                  <IconButton
                    edge="start"
                    color="inherit"
                    onClick={handleClose}
                    aria-label="close"
                  >
                    <CloseIcon />
                  </IconButton>
                  <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                    Iron Cactus (IRCA) User Guide
                  </Typography>
                </Toolbar>
              </AppBar>
              <List>
                <ListItemButton onClick={handleInst}>
                  <ListItemText primary="Instructions and Configurations" />
                  {openInst ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={!openInst} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemText sx={{ pl: 4 }}>
                      <ReactMarkdown>{instMd}</ReactMarkdown>
                    </ListItemText>
                  </List>
                </Collapse>
                <Divider />
                <ListItemButton onClick={handleDett} id="dettect">
                  <ListItemText primary="MITRE DeTT&CT" />
                  {openDett ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={!openDett} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemText sx={{ pl: 4 }}>
                      <ReactMarkdown>{detMd}</ReactMarkdown>
                    </ListItemText>
                  </List>
                </Collapse>
                <Divider />
                <ListItemButton onClick={handleAtt} id="attack">
                  <ListItemText primary="MITRE ATT&CK" />
                  {openAtt ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={!openAtt} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemText sx={{ pl: 4 }}>
                      <ReactMarkdown>{attMd}</ReactMarkdown>
                    </ListItemText>
                  </List>
                </Collapse>
                <Divider />
                <ListItemButton onClick={handleDef} id="defend">
                  <ListItemText primary="MITRE D3fend" />
                  {openDef ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={!openDef} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemText sx={{ pl: 4 }}>
                      <ReactMarkdown>{defMd}</ReactMarkdown>
                    </ListItemText>
                  </List>
                </Collapse>
              </List>
            </Dialog>
          </Toolbar>
        </AppBar>
      </Box>
    </ThemeContext.Provider>
  );
}
