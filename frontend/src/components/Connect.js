import React, { useState }  from 'react'
import Alert from '@mui/material/Alert'
import Box from '@mui/material/Box'
import Checkbox from '@mui/material/Checkbox'
import CloseIcon from '@mui/icons-material/Close'
import Collapse from '@mui/material/Collapse'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import FormControlLabel from '@mui/material/FormControlLabel'
import HelpIcon from '@mui/icons-material/Help'
import IconButton from '@mui/material/IconButton'
import MuiButton from '@mui/material/Button'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'


export const Button = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "7%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '76%'
}))

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export function HelpConnect() {
  const [open, setOpen] = React.useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon />
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Connect Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            User input: Host Address, Port Number, and Enable PIOC Box.
          </Typography>
          <Typography gutterBottom>
            Entering just the Host Address and Port Number will test your Elastic
            connection.
          </Typography>
          <Typography gutterBottom>
            Enabling PIOC will launch the process for finding Potential Indicators
            of Compromise (PIOC).
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  )
}

export function ConnectTab() {
  const [goodPopup, setGoodPopup] = useState(false)
  const [badPopup, setBadPopup] = useState(false)
  const [popupWords, setPopupWords] = useState('')
  const [hostAddress, setHostAddress] = useState('')
  const [portNumber, setPortNumber] = useState('9200')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [useSsl, setUseSsl] = useState(false)
  const [checked, setChecked] = useState(false)
  const [wait, setWait] = useState(false)
  const [submit, setSubmit] = useState('Connect')

  const sslChange = () => {
    setUseSsl(true)
    if(useSsl === true){
      setUseSsl(false)
    }
  }

  const handleChange = () => {
    setChecked(true)
    setSubmit('Submit')
    if(checked === true){
      setChecked(false)
      setSubmit('Connect')
    }
  }

  const onSubmit = e => {
    e.preventDefault()
    setBadPopup(false)
    setGoodPopup(false)
    setWait(true)

    let state = {
      hostAddress: hostAddress,
      portNumber: portNumber,
      username: username,
      password: password,
      useSsl: useSsl,
      checked: checked,
    }

    //Options for API request
    let options = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: 'POST',
      mode: 'no-cors',
      body: JSON.stringify(state)
    }

    fetch(window.location.href + `connect`, options)
      .then(res => {
        try{
          if(res.status === 200) {
            if(checked === true) {
              processData(state)
            }else{
              setGoodPopup(true)
              setBadPopup(false)
              setPopupWords('Connection Successful!')
              setWait(false)
            }
          }else{
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Connection Failed!')
            setWait(false)
          }
        }catch(error) {
          setGoodPopup(false)
          setBadPopup(true)
          setPopupWords('Connection Failed!')
          setWait(false)
      }})
  }

  const processData = state => {
    setGoodPopup(true)
    setBadPopup(false)
    setPopupWords('Connection Successful! Running PIOC')
    setWait(true)

    //Options for API request
    let options = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: 'POST',
      mode: 'no-cors',
      body: JSON.stringify(state),
    }

    fetch(window.location.href + `linkage`, options)
      .then(res => {
        try{
          if(res.status === 200) {
            setGoodPopup(true)
            setPopupWords('PIOC uploaded!')
            setBadPopup(false)
            setWait(false)
          }else{
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Failed PIOC!')
            setWait(false)
          }
        }catch(error) {
          setGoodPopup(false)
          setBadPopup(true)
          setPopupWords('Failed PIOC!')
          setWait(false)
      }})
  }
    return (
      <>
        <Box sx={{ width: '100%' }}>
          <Collapse in={goodPopup}>
            <Alert
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setGoodPopup(false)
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              sx={{ mb: 2 }}
            >
              {popupWords}
            </Alert>
          </Collapse>
          <Collapse in={badPopup}>
            <Alert severity="error"
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setBadPopup(false)
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              sx={{ mb: 2 }}
            >
              {popupWords}
            </Alert>
          </Collapse>
        </Box>
        <HelpConnect/>
        <form onSubmit={e => onSubmit(e)} className="form">
          <input
            className="hostAddress stringInput"
            name="hostAddress"
            placeholder="Host Address"
            required
            value={hostAddress}
            onChange={e => setHostAddress(e.target.value)}
          />
          <input
            className="portNumber stringInput"
            name="portNumber"
            placeholder="Port Number"
            required
            value={portNumber}
            onChange={e => setPortNumber(e.target.value)}
          />
          <input
            className="username stringInput"
            name="username"
            placeholder="Username"
            required
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
          <input
            className="password stringInput"
            type="password"
            name="password"
            placeholder="Password"
            required
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <FormControlLabel
            className="sslLabel"
            name="useSSL"
            label="Use HTTPS/SSL"
            control={<Checkbox checked={useSsl} onChange={e => sslChange(e)}
            color="default"/>}
          />
          <FormControlLabel
            className="piocLabel"
            name="enablePIOC"
            label="Enable PIOC"
            control={<Checkbox checked={checked} onChange={e => handleChange(e)}
            color="default"/>}
          />
          <Button
            variant="contained"
            type="submit"
            className="submit"
            disabled={wait}
            onClick={(e) => onSubmit(e)}
            id="submitConnect"
          >
            {submit}
          </Button>
        </form>
      </>
    )
  }

export default class Connect extends React.Component {
  render() {
    return (
      <>
        <ConnectTab/>
      </>
    )
  }
}