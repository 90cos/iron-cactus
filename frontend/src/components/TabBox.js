import React from 'react'
import Tabs from '@mui/material/Tabs'
import PropTypes from 'prop-types'
import Tab from '@mui/material/Tab'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import Connect from './Connect'
import CreateLayer from './CreateLayer'
import Upload from './Upload'
import Combinator from './Combinator'
import Layers from './Layers'
import PIOC from './PIOC'
import Stixi from './Stixi'


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tabpanel-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tabpanel-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ minWidth: '700px' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={value}
          onChange={handleChange}
          variant="fullWidth"
          textColor="inherit">
          <Tab label="Connect" {...a11yProps(0)} />
          <Tab label="Threat Group Layer" {...a11yProps(1)} />
          <Tab label="Upload" {...a11yProps(2)} />
          <Tab label="Layer Management" {...a11yProps(3)} />
          <Tab label="Combinator" {...a11yProps(4)} />
          <Tab label="PIOC Config" {...a11yProps(5)} />
          <Tab label="STIX Config" {...a11yProps(6)} />
        </Tabs>
      </Box>
      <Box>
        <TabPanel value={value} index={0}>
          <Connect/>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <CreateLayer/>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Upload/>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Layers/>
        </TabPanel>
        <TabPanel value={value} index={4}>
          <Combinator/>
        </TabPanel>
        <TabPanel value={value} index={5}>
          <PIOC/>
        </TabPanel>
        <TabPanel value={value} index={6}>
          <Stixi/>
        </TabPanel>
      </Box>
    </Box>
  );
}
