import React, { useState }  from 'react'
import Box from '@mui/material/Box'
import Alert from '@mui/material/Alert'
import Collapse from '@mui/material/Collapse'
import CloseIcon from '@mui/icons-material/Close'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import HelpIcon from '@mui/icons-material/Help'
import MuiButton from '@mui/material/Button'
import PropTypes from 'prop-types'
import IconButton from '@mui/material/IconButton'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import RestartAltIcon from '@mui/icons-material/RestartAlt'


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export function PIOCHelp() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon/>
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          PIOC Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
          PIOC or Potential Indicators of Compromise, is a way to query ElasticSearch based on sigma rules (https://github.com/SigmaHQ/sigma).
          </Typography>
          <Typography gutterBottom>
          This tab allows you to download the current sigma rules being used as well as upload custom sigma rules you wish to run against ElasticSearch.
          </Typography>
          <Typography gutterBottom>
          For example sigma rules, download the current PIOC config or go to https://github.com/SigmaHQ/sigma for more examples and information on sigma rules.
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  );
}

export const Button = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "12%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '76%'
}));

export const ResetButton = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "65%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '30%',
  backgroundColor: 'red',
}));


export function PIOC() {
  const [files] = useState([])
  const [goodPopup, setGoodPopup] = useState(false)
  const [badPopup, setBadPopup] = useState(false)
  const [popupWords, setPopupWords] = useState('')
  const [wait, setWait] = useState(false)


  const onSubmit = e => {
    e.preventDefault()
    setBadPopup(false)
    setPopupWords('Uploading Files')
    setGoodPopup(true)
    setWait(true)

    let data = new FormData()
    data.append('file', e.target.files[0]);

    //Options for API request
    let options = {
      method: 'POST',
      body: data,
    };

    fetch(window.location.href + `uploadpiocconfig`, options)
      .then(res => {
        try{
          if(res.status === 200) {
            setGoodPopup(true)
            setBadPopup(false)
            setPopupWords('Uploaded Files!')
            setWait(false)
          }else {
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Uploading Files Failed!')
            setWait(false)
          }}catch(error) {
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Uploading Files Failed!')
            setWait(false)
      }})
      e.target.value=null
  }

  const onDownload = e => {
    e.preventDefault();
    setBadPopup(false)
    setWait(true)

    fetch(window.location.href + 'getpiocconfig')
      .then(res => {
        try{
          if(res.status === 200) {
            window.open(window.location.href + 'getpiocconfig', '_blank');
            setGoodPopup(true)
            setBadPopup(false)
            setPopupWords('Download Successful!')
            setWait(false)
          }else{
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Download Failed!')
            setWait(false)}
          }catch(error) {
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('Download Failed!')
            setWait(false)
      }})
  }

  const onReset= e => {
    e.preventDefault();
    setBadPopup(false)
    setWait(true)

    //Options for API request
    let options = {
        method: 'POST',
    };

    fetch(window.location.href + 'resetpioc', options)
      .then(res => {
        try{
          if(res.status === 200) {
              setGoodPopup(true)
              setBadPopup(false)
              setPopupWords('PIOC Files Reset!')
              setWait(false)
            }else{
              setGoodPopup(false)
              setBadPopup(true)
              setPopupWords('PIOC Reset Failed!')
              setWait(false)
            }
          }catch(error) {
            setGoodPopup(false)
            setBadPopup(true)
            setPopupWords('PIOC Reset Failed!')
            setWait(false)
      }})
  }

  return (
    <>
      <Box sx={{ width: '100%' }}>
        <Collapse in={goodPopup}>
          <Alert
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setGoodPopup(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            {popupWords}
          </Alert>
        </Collapse>
        <Collapse in={badPopup}>
          <Alert severity="error"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setBadPopup(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            {popupWords}
          </Alert>
        </Collapse>
      </Box>
      <PIOCHelp/>
      <Button
        variant="contained"
        component="label"
        type="file"
        onChange={e => onSubmit(e)}
      >
        Upload New Config
        <input
          accept='.zip'
          multiple type="file"
          name={files}
          autoComplete='false'
          hidden
        />
      </Button>
      <br/><br/>
      <Button
        variant="contained"
        className="submitMed"
        value="Download"
        onClick={(e) => onDownload(e)}
        >Download Current Config
      </Button>
      <br/><br/>
      <br/><br/>
      <ResetButton
        startIcon={<RestartAltIcon />}
        variant="contained"
        value="Download"
        onClick={(e) => onReset(e)}
        disabled={wait}
        >
          Reset PIOC config to Defualt
      </ResetButton>
    </>
  );
}

export default class Connect extends React.Component {

  render() {
    return (
      <>
        <PIOC/>
      </>
    );
  }
}