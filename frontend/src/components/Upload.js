import React, { useState }  from 'react'
import Box from '@mui/material/Box'
import Alert from '@mui/material/Alert'
import Collapse from '@mui/material/Collapse'
import CloseIcon from '@mui/icons-material/Close'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import HelpIcon from '@mui/icons-material/Help'
import IconButton from '@mui/material/IconButton'
import MuiButton from '@mui/material/Button'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export function UploadHelp() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon/>
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Upload Layer Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Allowed files: Data Source, Techniques, Groups, ATT&CK Navigator Layers.
          </Typography>
          <Typography gutterBottom>
            Allowed extensions: .json, .yaml, and .yml
          </Typography>
          <Typography gutterBottom>
            Multiple file uploads on single multiple selection of files allowed.
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  );
}

export const Button = styled(MuiButton)(() => ({
  cursor: "pointer",
  borderRadius: "5em",
  color: "white",
  background: "#474747",
  border: "0",
  fontFamily: "Ubuntu, sans-serif",
  marginLeft: "12%",
  fontSize: "13px",
  boxShadow: "0 0 20px 1px rgba(0, 0, 0, 0.04)",
  textTransform: "None",
  minWidth: '76%'        
}));

export function Upload() {
  const [uploading, setUploading] = useState(false)
  const [successUpload, setSuccessUpload] = useState(false)
  const [failedUpload, setFailedUpload] = useState(false)
  const [wait, setWait] = useState(false)

  const onSubmit = e => {
    e.preventDefault()
    setUploading(true)
    setWait(true)

    let formdata = new FormData()

    for(let i = 0; i < e.target.files.length; i++){
      formdata.append(e.target.files[i].name, e.target.files[i])
    }

    //Options for API request
    let options = {
      method: 'POST',
      body: formdata,
    };

    fetch(window.location.href + `uploadfiles`, options)
      .then(res => {
        try{
          if(res.status === 200) {
            setSuccessUpload(true)
            setFailedUpload(false)
            setUploading(false)
            setWait(false)
          }else {
            setSuccessUpload(false)
            setFailedUpload(true)
            setUploading(false)
            setWait(false)
          }
        }catch(error) {
          setFailedUpload(true)
          setSuccessUpload(false)
          setUploading(false)
          setWait(false)
      }})
      e.target.value=null
  }

  return (
    <>
      <Box sx={{ width: '100%' }}>
      <Collapse in={uploading}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setUploading(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          Uploading Files
        </Alert>
      </Collapse>
      <Collapse in={successUpload}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setSuccessUpload(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          Uploaded Files!
        </Alert>
      </Collapse>
      <Collapse in={failedUpload}>
        <Alert severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setFailedUpload(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          Uploading Files Failed!
        </Alert>
      </Collapse>
      </Box>
      <UploadHelp/>
      <Button
        variant="contained"
        component="label"
        textColor="inherit"
        onChange={e => onSubmit(e)}
        disabled={wait}
      >
        Upload Files
        <input
          accept='.json,.yaml,.yml'
          autoComplete='false'
          hidden
          multiple type="file"
          name="files"
        />
      </Button>
    </>
  );
}

export default class Connect extends React.Component {
  
  render() {
    return (
      <>
        <Upload/>
      </>
    );
  }
}
