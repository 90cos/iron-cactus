import React, { useState, useEffect } from 'react'
import Alert from '@mui/material/Alert'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import CloseIcon from '@mui/icons-material/Close'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import HelpIcon from '@mui/icons-material/Help'
import IconButton from '@mui/material/IconButton'
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}))

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
}

export function LayersHelp() {
  const [open, setOpen] = React.useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <IconButton variant="outlined" onClick={handleClickOpen}>
        <HelpIcon/>
      </IconButton>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          Download Layer Tab
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
            Default layers that will populate Attack Navigator. Click download to 
            download the layer and click remove to delete a layer from Attack Navigator.
          </Typography>
          <Typography gutterBottom>
            Download: Downloads selected layer.
          </Typography>
          <Typography gutterBottom>
            Remove: Deletes selected layer.
          </Typography>
        </DialogContent>
      </BootstrapDialog>
    </div>
  )
}

export function Layer() {
  const [items, setItems] = useState([])
  const [badPopup, setBadPopup] = useState(false)
  const [popupWords, setPopupWords] = useState('')

  useEffect(() => {
    fetch(window.location.href + 'getdefaultlayers')
      .then((res) => res.json())
      .then((json) => {
        setItems(json["layers"])
      })
  }, [])

  const onDownload = e => {
    e.preventDefault()
    setBadPopup(false)

    //Options for API request
    let options = {
      headers: {
        Accept: "application/json",    
        "Content-Type": "application/json",
      },
      method: 'GET',
      mode: 'no-cors', 
    }
    fetch(window.location.href + 'downloadlayers?downloadfile=' + e.target.name, options)
      .then(res => {
        try{
          if(res.status === 200) {
            window.open(window.location.href + 'downloadlayers?downloadfile=' + e.target.name, '_blank')
          }else{
            setBadPopup(true)
            setPopupWords('Download Failed!')
          }
        }
        catch(error) { 
          setBadPopup(true)
          setPopupWords('Download Failed!')
      }})
  }

  const onRemove = e => {
    e.preventDefault()
    setBadPopup(false)

    //Options for API request
    let options = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: 'POST',
      mode: 'no-cors',
      body: JSON.stringify(e.target.name),
    }
    fetch(window.location.href + 'removedefaultlayer?removefile=' + e.target.name, options)
      .then(res => {
        try {
          if (res.status === 200) {
            fetch(window.location.href + 'getdefaultlayers')
              .then((res) => res.json())
              .then((json) => {
                setItems(json["layers"])
              })
          } else(
            alert("File not removed!")
          )} catch (error) {
            setBadPopup(true)
            setPopupWords('Download Failed!')
          }
        })
}
    return (
      <>
        <Box sx={{ width: '100%' }}>
          <Collapse in={badPopup}>
            <Alert severity="error"
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => setBadPopup(false)}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              sx={{ mb: 2 }}
            >
              {popupWords}
            </Alert>
          </Collapse>
        </Box>
        <LayersHelp/>
        <div>
          {(items && items.length === 0) ? <p className='noData'>There are no default layers to download.</p> : (
            <div className="management">
              <table>
                {items.map(layer => (
                  <tr key={layer}>
                    <td>{layer}</td>
                    <td>
                      <button
                        className="download"
                        name={layer}
                        value="Download"
                        onClick={(e) => onDownload(e)}
                      > Download</button>
                    </td>
                    <td>
                      <button
                        className="download"
                        name={layer}
                        value="Remove"
                        onClick={(e) => onRemove(e)}
                      > Remove</button>
                    </td>
                  </tr>
                ))}
              </table>
            </div>
          )}
        </div>          
      </>
    )
  }

  export default class Connect extends React.Component {
    render() {
      return (
        <>
          <Layer/>
        </>
      )
    }
  }
